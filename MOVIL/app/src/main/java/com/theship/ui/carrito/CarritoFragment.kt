package com.theship.ui.carrito

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.theship.R
import com.theship.api.ApiCliente
import com.theship.main.adapter.AdapterClub
import com.theship.main.adapter.AdapterCompras
import com.theship.model.club.Club
import com.theship.model.club.ListaCompra
import com.theship.model.club.comprasTotal
import com.theship.model.precioclub.precioClub
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList

class CarritoFragment : Fragment() {

    private val COMPRAS = "COMPRAS"
    var clubs: ArrayList<comprasTotal> = ArrayList()
    lateinit var recycler: RecyclerView
    lateinit var texttotal: TextView
    lateinit var textpagar: TextView
    lateinit var prosesarPago: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_carrito, container, false)
        recycler = root.findViewById(R.id.listaTotalCompras)
        recycler.layoutManager = LinearLayoutManager(activity)
        texttotal = root.findViewById(R.id.totalcomprado)
        textpagar = root.findViewById(R.id.totalPagar)
        prosesarPago = root.findViewById(R.id.procesarPago)

        recycler.adapter?.notifyDataSetChanged()
        obtenerCompras()

        return root


    }

    private fun obtenerCompras(): String {
        val settings = context?.getSharedPreferences(COMPRAS, 0)
        val body = settings?.getString("BODY", null)
        val mind = settings?.getString("MIND", null)
        val nutrition = settings?.getString("NUTRICIÓN ", null)
        val talleres = settings?.getString("TALLERES ", null)
        val reto = settings?.getString("RETO 21 DIAS ", null)


        //condicionamos que no viene los datos en null para insertar en el arrayList
        var idClub: ArrayList<comprasTotal> = ArrayList()

        if (body != null && mind != null && nutrition != null && talleres != null) {
            // Se procesa los datos que vienen en string body
            var b = body!!.replace("[", "")
            var b1 = b!!.replace("]", "")
            val bodyData = b1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string mind
            var m = mind!!.replace("[", "")
            var m1 = m!!.replace("]", "")
            val mindData = m1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string talleres
            var t = talleres!!.replace("[", "")
            var t1 = t!!.replace("]", "")
            val tallerData = t1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string nutrition
            var n = nutrition!!.replace("[", "")
            var n1 = n!!.replace("]", "")
            val nutricionData = n1!!.split("{").toTypedArray()

            idClub.add(comprasTotal(bodyData[0], bodyData[1], bodyData[2], bodyData[3]))
            idClub.add(comprasTotal(mindData[0], mindData[1], mindData[2], mindData[3]))
            idClub.add(
                comprasTotal(
                    nutricionData[0],
                    nutricionData[1],
                    nutricionData[2],
                    nutricionData[3]
                )
            )
            idClub.add(comprasTotal(tallerData[0], tallerData[1], tallerData[2], tallerData[3]))

            precioClub(4)
        } else if (body != null && mind != null && nutrition != null) {
            var b = body!!.replace("[", "")
            var b1 = b!!.replace("]", "")
            val bodyData = b1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string mind
            var m = mind!!.replace("[", "")
            var m1 = m!!.replace("]", "")
            val mindData = m1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string nutrition
            var n = nutrition!!.replace("[", "")
            var n1 = n!!.replace("]", "")
            val nutricionData = n1!!.split("{").toTypedArray()


            idClub.add(comprasTotal(bodyData[0], bodyData[1], bodyData[2], bodyData[3]))
            idClub.add(comprasTotal(mindData[0], mindData[1], mindData[2], mindData[3]))
            idClub.add(
                comprasTotal(
                    nutricionData[0],
                    nutricionData[1],
                    nutricionData[2],
                    nutricionData[3]
                )
            )
            precioClub(3)
        } else if (body != null && mind != null && talleres != null) {
            var b = body!!.replace("[", "")
            var b1 = b!!.replace("]", "")
            val bodyData = b1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string mind
            var m = mind!!.replace("[", "")
            var m1 = m!!.replace("]", "")
            val mindData = m1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string talleres
            var t = talleres!!.replace("[", "")
            var t1 = t!!.replace("]", "")
            val tallerData = t1!!.split("{").toTypedArray()


            idClub.add(comprasTotal(bodyData[0], bodyData[1], bodyData[2], bodyData[3]))
            idClub.add(comprasTotal(mindData[0], mindData[1], mindData[2], mindData[3]))
            idClub.add(comprasTotal(tallerData[0], tallerData[1], tallerData[2], tallerData[3]))
            precioClub(3)
        } else if (body != null && nutrition != null && talleres != null) {
            var b = body!!.replace("[", "")
            var b1 = b!!.replace("]", "")
            val bodyData = b1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string nutrition
            var n = nutrition!!.replace("[", "")
            var n1 = n!!.replace("]", "")
            val nutricionData = n1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string talleres
            var t = talleres!!.replace("[", "")
            var t1 = t!!.replace("]", "")
            val tallerData = t1!!.split("{").toTypedArray()

            idClub.add(comprasTotal(bodyData[0], bodyData[1], bodyData[2], bodyData[3]))
            idClub.add(
                comprasTotal(
                    nutricionData[0],
                    nutricionData[1],
                    nutricionData[2],
                    nutricionData[3]
                )
            )
            idClub.add(comprasTotal(tallerData[0], tallerData[1], tallerData[2], tallerData[3]))
            precioClub(3)
        } else if (body != null && mind != null) {
            var b = body!!.replace("[", "")
            var b1 = b!!.replace("]", "")
            val bodyData = b1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string mind
            var m = mind!!.replace("[", "")
            var m1 = m!!.replace("]", "")
            val mindData = m1!!.split("{").toTypedArray()

            idClub.add(comprasTotal(bodyData[0], bodyData[1], bodyData[2], bodyData[3]))
            idClub.add(comprasTotal(mindData[0], mindData[1], mindData[2], mindData[3]))

            precioClub(2)

        } else if (body != null && nutrition != null) {
            var b = body!!.replace("[", "")
            var b1 = b!!.replace("]", "")
            val bodyData = b1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string nutrition
            var n = nutrition!!.replace("[", "")
            var n1 = n!!.replace("]", "")
            val nutricionData = n1!!.split("{").toTypedArray()

            idClub.add(comprasTotal(bodyData[0], bodyData[1], bodyData[2], bodyData[3]))
            idClub.add(
                comprasTotal(
                    nutricionData[0],
                    nutricionData[1],
                    nutricionData[2],
                    nutricionData[3]
                )
            )

            precioClub(2)

        } else if (body != null && talleres != null) {
            var b = body!!.replace("[", "")
            var b1 = b!!.replace("]", "")
            val bodyData = b1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string talleres
            var t = talleres!!.replace("[", "")
            var t1 = t!!.replace("]", "")
            val tallerData = t1!!.split("{").toTypedArray()

            idClub.add(comprasTotal(bodyData[0], bodyData[1], bodyData[2], bodyData[3]))
            idClub.add(comprasTotal(tallerData[0], tallerData[1], tallerData[2], tallerData[3]))
            precioClub(2)

        } else if (mind != null && nutrition != null) {

            // Se procesa los datos que vienen en string mind
            var m = mind!!.replace("[", "")
            var m1 = m!!.replace("]", "")
            val mindData = m1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string nutrition
            var n = nutrition!!.replace("[", "")
            var n1 = n!!.replace("]", "")
            val nutricionData = n1!!.split("{").toTypedArray()


            idClub.add(comprasTotal(mindData[0], mindData[1], mindData[2], mindData[3]))
            idClub.add(
                comprasTotal(
                    nutricionData[0],
                    nutricionData[1],
                    nutricionData[2],
                    nutricionData[3]
                )
            )
            precioClub(2)
        } else if (mind != null && talleres != null) {
            // Se procesa los datos que vienen en string talleres
            var t = talleres!!.replace("[", "")
            var t1 = t!!.replace("]", "")
            val tallerData = t1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string mind
            var m = mind!!.replace("[", "")
            var m1 = m!!.replace("]", "")
            val mindData = m1!!.split("{").toTypedArray()


            idClub.add(comprasTotal(mindData[0], mindData[1], mindData[2], mindData[3]))
            idClub.add(comprasTotal(tallerData[0], tallerData[1], tallerData[2], tallerData[3]))
            precioClub(2)
        } else if (mind != null && nutrition != null) {
            // Se procesa los datos que vienen en string nutrition
            var n = nutrition!!.replace("[", "")
            var n1 = n!!.replace("]", "")
            val nutricionData = n1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string mind
            var m = mind!!.replace("[", "")
            var m1 = m!!.replace("]", "")
            val mindData = m1!!.split("{").toTypedArray()


            idClub.add(comprasTotal(mindData[0], mindData[1], mindData[2], mindData[3]))
            idClub.add(
                comprasTotal(
                    nutricionData[0],
                    nutricionData[1],
                    nutricionData[2],
                    nutricionData[3]
                )
            )
            precioClub(2)

        } else if (nutrition != null && talleres != null) {
            // Se procesa los datos que vienen en string nutrition
            var n = nutrition!!.replace("[", "")
            var n1 = n!!.replace("]", "")
            val nutricionData = n1!!.split("{").toTypedArray()

            // Se procesa los datos que vienen en string talleres
            var t = talleres!!.replace("[", "")
            var t1 = t!!.replace("]", "")
            val tallerData = t1!!.split("{").toTypedArray()


            idClub.add(
                comprasTotal(
                    nutricionData[0],
                    nutricionData[1],
                    nutricionData[2],
                    nutricionData[3]
                )
            )
            idClub.add(comprasTotal(tallerData[0], tallerData[1], tallerData[2], tallerData[3]))
            precioClub(2)
        } else if (body != null) {
            var b = body!!.replace("[", "")
            var b1 = b!!.replace("]", "")
            val bodyData = b1!!.split("{").toTypedArray()

            idClub.add(comprasTotal(bodyData[0], bodyData[1], bodyData[2], bodyData[3]))
            precioClub(1)
        } else if (nutrition != null) {
            // Se procesa los datos que vienen en string nutrition
            var n = nutrition!!.replace("[", "")
            var n1 = n!!.replace("]", "")
            val nutricionData = n1!!.split("{").toTypedArray()

            idClub.add(
                comprasTotal(
                    nutricionData[0],
                    nutricionData[1],
                    nutricionData[2],
                    nutricionData[3]
                )
            )
            precioClub(1)
        } else if (talleres != null) {
            // Se procesa los datos que vienen en string talleres
            var t = talleres!!.replace("[", "")
            var t1 = t!!.replace("]", "")
            val tallerData = t1!!.split("{").toTypedArray()

            idClub.add(comprasTotal(tallerData[0], tallerData[1], tallerData[2], tallerData[3]))
            precioClub(1)
        } else if (mind != null) {
            // Se procesa los datos que vienen en string mind
            var m = mind!!.replace("[", "")
            var m1 = m!!.replace("]", "")
            val mindData = m1!!.split("{").toTypedArray()

            idClub.add(comprasTotal(mindData[0], mindData[1], mindData[2], mindData[3]))
            precioClub(1)
        } else if (reto != null) {
            var rt = reto!!.replace("[", "")
            var rt1 = rt!!.replace("]", "")
            val retoData = rt1!!.split("{").toTypedArray()
            idClub.add(comprasTotal(retoData[0], retoData[1], retoData[2], retoData[3]))
            precioClub(0)
        }

        clubs = (idClub as ArrayList<comprasTotal>?)!!
        recycler.adapter = AdapterCompras(idClub as ArrayList<comprasTotal>)!!



        return body.toString()
    }


    fun precioClub(cantidad: Int) {
        val call: Call<ArrayList<precioClub?>>? =
            ApiCliente.getApiService()?.getPrecioClub(cantidad)
        call?.enqueue(object : Callback<ArrayList<precioClub?>> {
            override fun onResponse(
                call: Call<ArrayList<precioClub?>>,
                response: Response<ArrayList<precioClub?>>
            ) {
                if (response.isSuccessful) {

                    var dat = response.body()

                    textpagar.text = "Total a pagar: " + dat?.get(0)?.precio.toString()
                    texttotal.text = dat?.get(0)?.descripcion.toString()
                    prosesarPago.text = "Pagar"
                }
            }

            override fun onFailure(call: Call<ArrayList<precioClub?>?>, t: Throwable) {
                //  oKmutable.value = false
            }
        })


    }
}