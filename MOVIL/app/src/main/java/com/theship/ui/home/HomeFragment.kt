package com.theship.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.theship.R
import com.theship.api.ApiCliente
import com.theship.main.adapter.AdapterCoach
import com.theship.model.user.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

private lateinit var nombreUser:TextView
private lateinit var sexoUser:TextView
private lateinit var correoUser:TextView
private lateinit var edadUser:TextView
private lateinit var fechaRegistro:TextView
private lateinit var photoUser:ImageView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
   nombreUser = root.findViewById<TextView>(R.id.nombreUsuario)
   correoUser = root.findViewById<TextView>(R.id.emailUsuario)
   edadUser = root.findViewById<TextView>(R.id.edadUsuario)
   sexoUser = root.findViewById<TextView>(R.id.sexoUsuario)
   photoUser = root.findViewById<ImageView>(R.id.photoUsuario)
   fechaRegistro = root.findViewById<TextView>(R.id.fecharRegistro)


        homeViewModel.text.observe(viewLifecycleOwner, Observer {

        })
        val id =this.getArguments()?.getInt("id",0)?.toInt()

        val token = this.getArguments()?.getString("token")?.toString()

        val call: Call<ArrayList<User?>>? = ApiCliente.getApiService()?.getUser(token,id)
        call?.enqueue(object : Callback<ArrayList<User?>> {
            override fun onFailure(call: Call<ArrayList<User?>>?, t: Throwable) {
                var error = t.message.toString()
            }

            override fun onResponse(
                call: Call<ArrayList<User?>>?,
                response: Response<ArrayList<User?>>?,
            ) {
                if (response != null) {
                    if (response.isSuccessful) {
                        var dat = response.body()
                  nombreUser.text= dat?.get(0)?.name.toString()
               //   edadUser.text= dat?.get(0)?.e.toString()
                 // sexoUser.text= dat?.get(0)?.sex.toString()
                        var fech = SimpleDateFormat("EEE dd MMMM yyyy")
                        val fechaInicio = fech.format(dat?.get(0)?.createdAt)
                  correoUser.text= dat?.get(0)?.email.toString()

                        fechaRegistro.text = fechaInicio
                        Picasso.get().load( dat?.get(0)?.photoUser.toString()).into(photoUser)

                    //convierte a random los resultados antes de enviar al adaptador
                      //  Collections.shuffle(dat)

                     //   coach = (dat as ArrayList<User>?)!!
                    //    grid.adapter = AdapterCoach(activity!!.applicationContext,dat as ArrayList<User>)

                        //*  val res = arrayListOf()

                        //      Toast.makeText(this@DrawerMenu, res.toArray().toString(), Toast.LENGTH_SHORT)
                        //        .show()
                        Log.i("hola lista", Gson().toJson(dat))

                    }
                }

            }
        })
        return root
    }
}