package com.theship.ui.coach

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import com.google.gson.Gson
import com.theship.R
import com.theship.api.ApiCliente
import com.theship.main.adapter.AdapterCoach
import com.theship.main.adapter.VideoAdapter
import com.theship.model.contenido.Contenido
import com.theship.model.user.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class CoachFragment : Fragment() {

    companion object {
        fun newInstance() = CoachFragment()
    }

    private lateinit var viewModel: CoachViewModel
    var coach: ArrayList<User> = ArrayList()
    private lateinit var grid: GridView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.coach_fragment, container, false)


    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(CoachViewModel::class.java)
        grid = view.findViewById(R.id.grid2)

        val token = this.getArguments()?.getString("token")?.toString()


        val call: Call<ArrayList<User?>>? = ApiCliente.getApiService()?.getCoaches(token)
        call?.enqueue(object : Callback<ArrayList<User?>> {
            override fun onFailure(call: Call<ArrayList<User?>>?, t: Throwable) {
                var error = t.message.toString()
            }

            override fun onResponse(
                call: Call<ArrayList<User?>>?,
                response: Response<ArrayList<User?>>?,
            ) {
                if (response != null) {
                    if (response.isSuccessful) {
                        var dat = response.body()

//convierte a random los resultados antes de enviar al adaptador
                        Collections.shuffle(dat)

                        coach = (dat as ArrayList<User>?)!!
                        grid.adapter = AdapterCoach(activity!!.applicationContext,dat as ArrayList<User>)

                        //*  val res = arrayListOf()

                        //      Toast.makeText(this@DrawerMenu, res.toArray().toString(), Toast.LENGTH_SHORT)
                        //        .show()
                        Log.i("hola lista", Gson().toJson(dat))

                    }
                }

            }
        })
    }

}