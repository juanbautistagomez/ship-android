package com.theship.ui.suscripcion

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import com.theship.R
import com.theship.viewmodel.SuscripcionViewModel

class SuscripcionFragment : Fragment() {

    companion object {
        fun newInstance() = SuscripcionFragment()
    }


    private lateinit var viewModel: com.theship.viewmodel.SuscripcionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.suscripcion_fragment, container, false)
        viewModel =
            ViewModelProvider(this).get(SuscripcionViewModel::class.java)
        viewModel.recycler = root.findViewById(R.id.pagoSuscripcion) as GridView
        viewModel.traerClubs(context)

        return root
    }


}