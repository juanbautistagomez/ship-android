package com.theship.ui.slideshow

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.VideoView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.theship.R

class SlideshowFragment : Fragment() {

    private lateinit var slideshowViewModel: SlideshowViewModel
private  lateinit var textT:TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        slideshowViewModel =
                ViewModelProvider(this).get(SlideshowViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_envivo, container, false)
        textT= root.findViewById<TextView>(R.id.titleVideo)

//        val index:Int = args!!.getInt("id", 0)

        textT.text = this.getArguments()?.getInt("id",0).toString()
        var urlVideo: VideoView = root.findViewById(R.id.videoFreeSuscripcion2)
        //  urlVideo.setVideoPath("https://www.youtube.com/watch?v=9Wu9w7DpYSg.mp4")
        val vid_url = "https://assets.mixkit.co/videos/preview/mixkit-behind-the-scenes-of-a-speaker-talking-on-camera-34486-large.mp4"

        urlVideo.setVideoPath(vid_url)

        urlVideo.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp: MediaPlayer? ->
            mp?.start()
            // progressBar.visibility= View.GONE
            /*  val videoRatio = mp!!.videoWidth / mp!!.videoHeight.toFloat()
              val screenRatio = urlVideo.width / urlVideo.height.toFloat()
              val scale = videoRatio / screenRatio
              if (scale >= 1f) {
                  urlVideo.scaleX = scale
                  //urlVideo.setScaleX(scale);

              } else {
                  urlVideo.scaleY = 1f / scale
                  //urlVideo.setScaleY(1f/scale)
              }
  */

        })
        urlVideo.setOnCompletionListener(MediaPlayer.OnCompletionListener { mp: MediaPlayer? ->
            mp?.start()


        })

        return root
    }
}