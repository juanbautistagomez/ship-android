package com.theship.api

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiCliente {
    private var API_SERVICE: ApiTheShip? = null

    fun getApiService(): ApiTheShip? {

        // Creamos un interceptor y le indicamos el log level a usar
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        // Asociamos el interceptor a las peticiones
        val httpClient = OkHttpClient.Builder()
                .readTimeout(Constans().TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(Constans().TIME_OUT, TimeUnit.SECONDS)
        httpClient.addInterceptor(logging)

        var BASE_URL: String = Constans().URL_BASE_DEVELOPMENT
        if (API_SERVICE == null) {  // Patrón Singleton
            val gson = GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                    .create()
            val retrofit: Retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build()) // <-- usamos el log level
                    .build()
            API_SERVICE = retrofit.create(ApiTheShip::class.java)
        }
        return API_SERVICE

    }

    class Constans {

        val URL_BASE_DEVELOPMENT = "http://192.168.1.127:4000/";
        val TIME_OUT:Long = 6 ;
        val ITEM_COURSE = "itemCourse"
        val REQUEST_TIMEOUT_ERROR_MESSAGE = "La solicitud está tardando demasiado. Por favor inténtalo nuevamente."
        val DEFAULT_ERROR_CODE = 0
        val DEFAULT_ERROR = "Ha ocurrido un error, intentalo nuevamente."
        val UNAUTHORIZED_ERROR_CODE = 401
        val NOT_FOUND_ERROR_CODE = 404
    }
}
