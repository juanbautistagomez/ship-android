package com.theship.api

import com.theship.model.Registro.Registro
import com.theship.model.club.Club
import com.theship.model.club.Producto
import com.theship.model.contenido.Contenido
import com.theship.model.login.UserLogin
import com.theship.model.login.TokenResponse
import com.theship.model.precioclub.precioClub
import com.theship.model.suscriptores.SuscriptoresResponse
import com.theship.model.user.ResponseGeneral
import com.theship.model.user.User
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiTheShip {

    // @Headers("Content-Type: application/json")
    @POST("auth/login")
    fun login(@Body login: UserLogin?): Call<TokenResponse?>?

    // @Headers("Content-Type: application/json")
    @POST("auth/refresh")
    fun tokenRefresh(@Header("refresh") tokenRefresh: String): Call<TokenResponse?>?


    @POST("users")

    fun registro(@Body registro: Registro?): Call<ResponseGeneral?>?

    @GET("users/{id}")
    fun getUser(@Header("auth") token: String?, @Path("id") id: Int?): Call<ArrayList<User?>>?

    //
    @GET("users")
    fun getUsers(@Header("auth") token: String?): Call<ResponseBody?>?

    //club
    @GET("club")
    fun getClubs(): Call<ArrayList<Club?>>?

    //contenido
    @GET("contenido")
    fun getContenidoGratuito(@Header("auth") token: String?): Call<ArrayList<Contenido?>>?

    //producto
    @GET("producto")
    fun getProductos(@Header("auth") token: String?): Call<ArrayList<Producto?>>?

    //producto
    @GET("coach")
    fun getCoaches(@Header("auth") token: String?): Call<ArrayList<User?>>?



    //Suscripcion status
    @GET("suscriptores/{id}")
    fun getSuscriptores(@Header("auth") token: String?,@Path("id") id: Int?): Call<SuscriptoresResponse?>

    //Precio club
    @GET("precioclub/{cantidad}")
    fun getPrecioClub(@Path("cantidad") cantidad: Int?): Call<ArrayList<precioClub?>>


}