package com.theship.model.suscriptores

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SuscriptoresResponse:Serializable {

    @SerializedName("message")
    @Expose
    private val message: String? = null

}