package com.theship.model.coaches

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Coaches {

    @SerializedName("idcoaches")
    @Expose
    var idcoaches: Int? = null

    @SerializedName("nameCoaches")
    @Expose
    var nameCoaches: String? = null


}