package com.theship.model.role

import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(primaryKeys = ["idrole"])
class Role(

    @field:SerializedName("idrole")
    private val idrole: Int? = null,

    @field:SerializedName("nombreRole")
    private val nombreRole: String? = null

)