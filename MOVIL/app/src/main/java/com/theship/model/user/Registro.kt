package com.theship.model.Registro


class Registro {

    var name: String? = null
    var apellidos: String? = null
    var fechaNacimiento: String? = null
    var whatsapp: String? = null
    var email: String? = null
    var password: String? = null
    var role: Int? = null

}