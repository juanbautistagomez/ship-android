package com.theship.model.pago

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class Pago:Serializable {

    @SerializedName("montoPago")
    @Expose
    var montoPago:Int? = null

    @SerializedName("fechaPago")
    @Expose
    var fechaPago: Date? = null


    @SerializedName("fechaPago")
    @Expose
    var statusPago:Boolean? = null



}