package com.theship.model.user

import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.theship.model.role.Role
import retrofit2.http.Field
import java.io.Serializable
import java.util.*

@Entity(primaryKeys = ["id"])
data class User(

    @field:SerializedName("id")
    private val id: Int?,

    @field:SerializedName("name")
     val name: String?,

    @field:SerializedName("apellidos")
     val apellidos: String?,

    @field:SerializedName("photoUser")
    val photoUser: String? ,

    @field:SerializedName("nombreRole")
    val nombreRole: String? ,

    @field:SerializedName("email")
     val email: String?,

    @field:SerializedName("createdAt")
     var createdAt: Date?,

    @field:SerializedName("updateAt")
     val updateAt: String?,

    @field:SerializedName("role")
     val role: Role?,


    )