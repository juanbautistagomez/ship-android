package com.theship.model.precioclub

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class precioClub: Serializable {

    @SerializedName("idclubprecio")
    @Expose
    var idclubprecio: Int? = null

    @SerializedName("precio")
    @Expose
    var precio: Int? = null

    @SerializedName("descripcion")
    @Expose
    var descripcion: String? = null

}