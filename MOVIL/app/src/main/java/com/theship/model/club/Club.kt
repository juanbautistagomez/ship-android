package com.theship.model.club

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class Club : Serializable {

    @SerializedName("idclub")
    @Expose
    var idclub: Int? = null

    @SerializedName("nombreClub")
    @Expose
    var nombreClub: String? = null

    @SerializedName("wellnescombo")
    @Expose
    var wellnescombo: Boolean? = null

    @SerializedName("descripcionClub")
    @Expose
    var descripcionClub: String? = null

    @SerializedName("fechaInicioClub")
    @Expose
    var fechaInicioClub: Date? = null


    @SerializedName("urlPromocionClub")
    @Expose
    var urlPromocionClub: String? = null

    @SerializedName("urlImagenClub")
    @Expose
    var urlImagenClub: String? = null
}