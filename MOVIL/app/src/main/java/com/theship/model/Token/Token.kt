package com.theship.model.Token

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(primaryKeys = ["tokenRefresh"])
class Token  (

    @field:SerializedName("message")
    private val message: String?,

    @field:SerializedName("token")
    private val token: String?,

    @field:SerializedName("tokenRefresh")
    private val tokenRefresh: String?,



    )