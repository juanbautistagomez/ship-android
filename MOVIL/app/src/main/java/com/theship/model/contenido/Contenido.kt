package com.theship.model.contenido

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Contenido:Serializable {

    @SerializedName("contenido_idcontenido")
    @Expose
    var contenido_idcontenido: Int? = null

    @SerializedName("contenido_title")
    @Expose
    var contenido_title: String? = null

    @SerializedName("user_name")
    @Expose
    var user_name: String? = null

    @SerializedName("user_photoUser")
    @Expose
    var user_photoUser: String? = null

    @SerializedName("contenido_subtitle")
    @Expose
    var contenido_subtitle: String? = null

    @SerializedName("contenido_description")
    @Expose
    var contenido_description: String? = null

    @SerializedName("contenido_gratuito")
    @Expose
    var contenido_gratuito: Int? = null

    @SerializedName("contenido_thumb")
    @Expose
    var contenido_thumb: String? = null

    @SerializedName("contenido_sources")
    @Expose
    var contenido_sources: String? = null

    @SerializedName("fechaCreated")
    @Expose
    var fechaCreated: String? = null

}