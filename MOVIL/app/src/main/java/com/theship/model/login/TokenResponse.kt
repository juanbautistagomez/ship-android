package com.theship.model.login

import androidx.room.Entity
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
@Entity(primaryKeys = ["tokenRefresh"])
class TokenResponse : Serializable {

    @field:SerializedName("token")
    @Expose
    private var token: String? = null
    private var tokenRefresh: String? = null

    @field:SerializedName("id")
    private var id: Int? = null

    fun getToken(): String? {
        return token
    }

    fun setToken(token: String?) {
        this.token = token
    }

    fun getTokenRefresh(): String? {
        return tokenRefresh
    }

    fun setTokenRefresh(tokenRefresh: String?) {
        this.tokenRefresh = tokenRefresh
    }


    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

}