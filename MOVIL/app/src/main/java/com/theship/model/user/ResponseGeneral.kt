package com.theship.model.user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ResponseGeneral :Serializable {

    @SerializedName("code")
    @Expose
    private val code: Int? = null
}