package com.theship.model.user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.theship.model.role.Role
import java.io.Serializable

class UserResponse:Serializable {

    @SerializedName("id")
    @Expose
    private val id: Int? = null

    @SerializedName("name")
    @Expose
    private val name: String? = null

    @SerializedName("apellidos")
    @Expose
    private val apellidos: Any? = null

    @SerializedName("email")
    @Expose
    private val email: String? = null

    @SerializedName("createdAt")
    @Expose
    private val createdAt: String? = null

    @SerializedName("password")
    @Expose
    private val password: String? = null

    @SerializedName("updateAt")
    @Expose
    private val updateAt: String? = null

    @SerializedName("role")
    @Expose
    private val role: Role? = null

}