package com.theship.model.club

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class Producto:Serializable {

    @SerializedName("producto_idproducto")
    @Expose
    var producto_idproducto: Int? = null

    @SerializedName("producto_nombreProducto")
    @Expose
    var producto_nombreProducto: String? = null

    @SerializedName("producto_thumbProducto")
    @Expose
    var producto_thumbProducto: String? = null



    @SerializedName("producto_precioProducto")
    @Expose
    var producto_precioProducto: Double? = null

    @SerializedName("producto_detalleProducto")
    @Expose
    var producto_detalleProducto: String? = null

    @SerializedName("producto_stockProducto")
    @Expose
    var producto_stockProducto: Int? = null
}