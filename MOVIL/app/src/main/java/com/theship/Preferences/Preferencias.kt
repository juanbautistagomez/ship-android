package com.theship.Preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.NonNull
import com.theship.R

class Preferencias {
    private var PREFERENCIAS: SharedPreferences? = null

     fun get(@NonNull ctx: Context?): SharedPreferences? {
         instanciar(ctx!!)
         return PREFERENCIAS
    }
    fun getEditor(@NonNull ctx: Context?): SharedPreferences.Editor? {
        instanciar(ctx!!)
        return PREFERENCIAS?.edit()
    }

    private fun instanciar(@NonNull ctx: Context) {
        if (PREFERENCIAS == null) // patrón singleton
            PREFERENCIAS =
                ctx.getSharedPreferences(definirNombre(ctx), Context.MODE_PRIVATE)
    }

    private fun definirNombre(@NonNull ctx: Context): String? {
        return ctx.getString(R.string.app_name)
    }
}