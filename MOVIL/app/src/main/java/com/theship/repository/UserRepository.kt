package com.theship.repository

import androidx.lifecycle.LiveData
import com.theship.AppExecutors
import com.theship.api.ApiResponse
import com.theship.api.ApiTheShip
import com.theship.db.UserDAO
import com.theship.model.user.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private  val appExecutors: AppExecutors,
    private val userDao:UserDAO,
    private  val apiTheShip:ApiTheShip

){
fun loadLoginUser(id:String): LiveData<Resource<User>>{
    return  object: NetworkBoundResource<User,User>(appExecutors){
        override fun saveCallResult(item: User) {
            TODO("Not yet implemented")
        }

        override fun shouldFetch(data: User?): Boolean {
          return data == null
        }

        override fun loadFromDB(): LiveData<User> {
         return   userDao.findByLogin(id)
        }

        override fun createCall(): LiveData<ApiResponse<User>> {
            TODO("Not yet implemented")
        }

    }.asLiveData()
}

}