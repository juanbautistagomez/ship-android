package com.theship.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.theship.AppExecutors
import com.theship.api.ApiEmptyResponse
import com.theship.api.ApiErrorResponse
import com.theship.api.ApiResponse
import com.theship.api.ApiSuccessResponse

abstract class NetworkBoundResource <ResultType, RequestType>
@MainThread constructor(private  val appExecutors: AppExecutors){
    private val result = MediatorLiveData<Resource<ResultType>>()

    init {
        result.value = Resource.loading(null)
        val dbSource = loadFromDB()
        result.addSource(dbSource){ data ->
            result.removeSource(dbSource)
            if (shouldFetch(data)){
                fetchFromNetwork(dbSource)
            }else{
                result.addSource(dbSource){newData->
                    setValue(Resource.success(newData))
                }
            }

        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>){
        if(result.value != newValue){
            result.value = newValue
        }
    }

    private fun fetchFromNetwork(dbSource: LiveData<ResultType>){
val apiResponse: LiveData<ApiResponse<RequestType>> = createCall()
        result.addSource(dbSource){newData->
            setValue(Resource.loading(newData))
        }
result.addSource(apiResponse){response->
    result.removeSource(apiResponse)
    result.removeSource(dbSource)
    when(response){
        is ApiSuccessResponse->{
            appExecutors.diskIO().execute{
                saveCallResult(proccesResponse(response))
                appExecutors.mainThread().execute{
                    result.addSource(loadFromDB()){newData->
                        setValue(Resource.success(newData))
                    }
                }
            }
        }
        is ApiEmptyResponse->{
            appExecutors.mainThread().execute{
                result.addSource(loadFromDB()){newData->
                    setValue(Resource.success(newData))

                }
            }
        }
        is ApiErrorResponse->{
            onFecthFailed()
            result.addSource(dbSource){ newData->
                setValue(Resource.error((response as ApiErrorResponse).errorMessage, newData))
            }
        }
    }
}
    }

    protected  open fun onFecthFailed (){}

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    @WorkerThread
    protected  open fun  proccesResponse(response:ApiSuccessResponse<RequestType>) = response.body

    @WorkerThread
    protected abstract fun saveCallResult(item: RequestType)

    @MainThread
    protected abstract fun shouldFetch(data:ResultType?):Boolean

    @MainThread
    protected abstract fun loadFromDB():LiveData<ResultType>

    @MainThread
    protected abstract fun createCall():LiveData<ApiResponse<RequestType>>

}