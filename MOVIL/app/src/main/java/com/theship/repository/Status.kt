package com.theship.repository

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}