package com.theship.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.theship.api.ApiCliente
import com.theship.model.login.TokenResponse
import com.theship.model.login.UserLogin
import com.theship.model.suscriptores.SuscriptoresResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel : ViewModel() {

    private val _progressVisibility = MutableLiveData<Boolean>()
    val progressVisibility: LiveData<Boolean> get() = _progressVisibility
    var token: String? = null

    var tokenValido: String? = null
    var suscripcion: String? = null

    private val _messageVisibility = MutableLiveData<String>()
    val messageVisibility: LiveData<String> get() = _messageVisibility

    private var oKmutable = MutableLiveData<Boolean>()


    fun onButtonClicked(email: String, password: String) {


        viewModelScope.launch {

            if (email.isNotEmpty() && password.isNotEmpty()) {
                _progressVisibility.value = true
                login(password, email)
                _messageVisibility.value = withContext(Dispatchers.IO) {
                    Thread.sleep(2000)

                    "Verificando"

                }!!
            } else {
                _messageVisibility.value = withContext(Dispatchers.IO) {
                    Thread.sleep(2000)

                    "!!upps Los datos son necesarios"


                }!!
            }

            _progressVisibility.value = false


        }

    }


    fun getOkMutable(): LiveData<Boolean> {
        if (oKmutable == null) {
            oKmutable = MutableLiveData()
        }
        return oKmutable
    }


    fun login(password: String, email: String) {
        val login = UserLogin(email, password)
        val call: Call<TokenResponse?>? = ApiCliente.getApiService()?.login(login)
        call?.enqueue(object : Callback<TokenResponse?> {
            override fun onResponse(
                call: Call<TokenResponse?>,
                response: Response<TokenResponse?>
            ) {
                if (response.isSuccessful) {
                    token = response.body()?.getTokenRefresh()
                    //  var  tokenValido = token
                    //  oKmutable.value = true
                    tokenRefresh(token)
                }
            }

            override fun onFailure(call: Call<TokenResponse?>, t: Throwable) {
                oKmutable.value = false

            }

        })
    }

    //refresca el token para consultar usando el token refresh
    fun tokenRefresh(tokenRefresh: String?) {
        val call: Call<TokenResponse?>? =
            ApiCliente.getApiService()?.tokenRefresh(tokenRefresh.toString())
        call?.enqueue(object : Callback<TokenResponse?> {
            override fun onResponse(
                call: Call<TokenResponse?>,
                response: Response<TokenResponse?>
            ) {
                if (response.isSuccessful) {
                    var token = response.body()?.getToken()
                    tokenValido = tokenRefresh
                    //  var token3 = tokenValido
                    var idUser = response.body()?.getId()!!
                    suscripcion(token, idUser)

                }
                //   Toast.makeText(this@DrawerMenu, "Se ha generado un nuevo  token   !", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<TokenResponse?>, t: Throwable) {
                oKmutable.value = false
            }
        })
    }


    fun suscripcion(token: String?, idUser: Int) {
        val call: Call<SuscriptoresResponse?>? =
            ApiCliente.getApiService()?.getSuscriptores(token, idUser)
        call?.enqueue(object : Callback<SuscriptoresResponse?> {
            override fun onResponse(
                call: Call<SuscriptoresResponse?>,
                response: Response<SuscriptoresResponse?>
            ) {

                if (response.code() == 404) {
                    oKmutable.value = false

                    suscripcion = "false"
                } else if (response.code() == 200) {
                    oKmutable.value = true
                }


                //   Toast.makeText(this@DrawerMenu, "Se ha generado un nuevo  token   !", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<SuscriptoresResponse?>, t: Throwable) {

            }
        })
    }

    /* fun tokenRefresh(tokenRefresh: String?) {
         val call: Call<TokenResponse?>? = ApiCliente.getApiService()?.tokenRefresh(tokenRefresh.toString())
         call?.enqueue(object : Callback<TokenResponse?> {
             override fun onResponse(
                 call: Call<TokenResponse?>,
                 response: Response<TokenResponse?>
             ) {
                 if (response.isSuccessful) {
                     tokenValido = response.body()?.getToken()
                     var token3 = tokenValido
                     idUser = response.body()?.getId()
                     oKmutable.value = true


                     // guardarToken(token.toString());
                     //progressDialog!!.cancel()
                     /*if (obtenerToken() == token) {
                         irMain()
                     } else {
                         return
                     }*/
                 }
                     //   Toast.makeText(this@MainActivity, "Bienvenido !", Toast.LENGTH_SHORT)
                 // .show()
             }

             override fun onFailure(call: Call<TokenResponse?>, t: Throwable) {
                 oKmutable.value = false
             }
         })
     }*/

}