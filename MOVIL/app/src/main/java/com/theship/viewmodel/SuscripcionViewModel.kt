package com.theship.viewmodel

import android.content.Context
import android.widget.GridView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.theship.api.ApiCliente
import com.theship.main.adapter.AdapterClub
import com.theship.model.club.Club
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SuscripcionViewModel : ViewModel() {

    private val _progressVisibility = MutableLiveData<Boolean>()
    val progressVisibility: LiveData<Boolean> get() = _progressVisibility

    //message
    private val _messageVisibility = MutableLiveData<String>()
    val messageVisibility: LiveData<String> get() = _messageVisibility
    var tokenValido: String? = null
    var clubs: ArrayList<Club> = ArrayList()
    lateinit var recycler: GridView

    //refresca el token para consultar usando el token refresh


    fun traerClubs(context: Context?) {

        val call: Call<ArrayList<Club?>>? = ApiCliente.getApiService()?.getClubs()
        call?.enqueue(object : Callback<ArrayList<Club?>> {
            override fun onResponse(
                call: Call<ArrayList<Club?>>?,
                response: Response<ArrayList<Club?>>
            ) {
                if (response.isSuccessful) {
                    var token = response.body()

                    var dat = response.body()!!

                    clubs = (dat as ArrayList<Club>?)!!
                    recycler.adapter = AdapterClub(context!!, dat as ArrayList<Club>)!!


                }
                //   Toast.makeText(this@DrawerMenu, "Se ha generado un nuevo  token   !", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<ArrayList<Club?>>?, t: Throwable) {
                //  oKmutable.value = false
            }
        })

    }

}

