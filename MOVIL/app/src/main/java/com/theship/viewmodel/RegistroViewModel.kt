package com.theship.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.theship.api.ApiCliente
import com.theship.model.Registro.Registro
import com.theship.model.user.ResponseGeneral
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegistroViewModel : ViewModel() {

    private val _progressVisibility = MutableLiveData<Boolean>()
    val progressVisibility: LiveData<Boolean> get() = _progressVisibility

    //message
    private val _messageVisibility = MutableLiveData<String>()
    val messageVisibility: LiveData<String> get() = _messageVisibility


    //message passwor compared
    private val _messageVisibilityPassword = MutableLiveData<String>()
    val messageVisibilityPassword: LiveData<String> get() = _messageVisibilityPassword

    // action mutable register result //

    private val resultMutable = MutableLiveData<String>()
    private var oKmutable = MutableLiveData<Boolean>()


    fun getResultMutable(): LiveData<String> {
        if (resultMutable == null) {

        }
        return resultMutable
    }

    fun getOkMutable(): LiveData<Boolean> {
        if (oKmutable == null) {
            oKmutable = MutableLiveData()
        }
        return oKmutable
    }

    fun onButtonClickedRegister(
        name: String,
        apellidos: String,
        fechaNacimiento: String,
        whatsapp: String,
        email: String,
        passwordLogin: String,
        password: String
    ) {

        viewModelScope.launch {

            //comparamos que coincila la contraseña
            if (passwordLogin.equals(password)) {

                _messageVisibilityPassword.value = withContext(Dispatchers.IO) {
                    Thread.sleep(2000)

                    "La contraseña si coinciden"

                }!!

                //entra los datos


                if (name.isNotEmpty() && apellidos.isNotEmpty() && fechaNacimiento.isNotEmpty() && whatsapp.isNotEmpty() && email.isNotEmpty() && passwordLogin.isNotEmpty() && password.isNotEmpty()) {
                    _progressVisibility.value = true
                    registrarPost(name, apellidos, fechaNacimiento, whatsapp, email, password)
                    _messageVisibility.value = withContext(Dispatchers.IO) {
                        Thread.sleep(2000)

                        "Verificando"

                    }!!
                } else {
                    _messageVisibility.value = withContext(Dispatchers.IO) {
                        Thread.sleep(2000)

                        "!!upps Los datos son necesarios"


                    }!!
                }
                _progressVisibility.value = false

            } else {

                _messageVisibilityPassword.value = withContext(Dispatchers.IO) {
                    Thread.sleep(2000)

                    "No coincide la contraseña"

                }!!

            }
            _progressVisibility.value = false
        }
    }


    fun registrarPost(
        name: String,
        apellidos: String,
        fechaNacimiento: String,
        whatsapp: String,
        email: String,
        password: String
    ) {

        val registerUser = Registro()
        registerUser.name = name
        registerUser.apellidos = apellidos
        registerUser.fechaNacimiento = fechaNacimiento
        registerUser.whatsapp = whatsapp
        registerUser.email = email
        registerUser.password = password
        registerUser.role = 1


        val call: Call<ResponseGeneral?>? = ApiCliente.getApiService()?.registro(registerUser)
        call?.enqueue(object : Callback<ResponseGeneral?> {
            override fun onResponse(
                call: Call<ResponseGeneral?>,
                response: Response<ResponseGeneral?>
            ) {
                //  var respuesta = response.code()
                if (response.isSuccessful) {
                    oKmutable.value = true
                }

            }

            override fun onFailure(call: Call<ResponseGeneral?>, t: Throwable) {
                oKmutable.value = false
            }
        })
    }
}