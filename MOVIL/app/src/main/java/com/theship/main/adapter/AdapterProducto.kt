package com.theship.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.theship.R
import com.theship.model.club.Club
import com.theship.model.club.Producto
import java.text.SimpleDateFormat

class AdapterProducto(var producto: ArrayList<Producto>?) : RecyclerView.Adapter<AdapterProducto.RecyclerViewHolder>() {

    var context: Context? = null

    override fun getItemCount(): Int {
        return producto!!.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindData(producto, position)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        var view: View = LayoutInflater.from(parent.context).inflate(R.layout.recyclerviewproducto,
                parent,
                false)
        return RecyclerViewHolder(view)
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textNameProducto: TextView = itemView.findViewById(R.id.nombreProducto)
        var textdescripcionProducto: TextView = itemView.findViewById(R.id.descripcionProducto)
        var textprecioProducto: TextView = itemView.findViewById(R.id.precioProducto)
        var imgProducto: ImageView = itemView.findViewById(R.id.imgProducto)
       // var textfechaInicioClub: TextView = itemView.findViewById(R.id.fechaInicioClub)

        //  var opponentImage: TextView = itemView.findViewById(R.id.opponent_image)

        fun bindData(producto: ArrayList<Producto>?, position: Int) {

           // var fech = SimpleDateFormat("EEE dd MMMM yyyy")
          //  var fechaInicio = fech.format(clubs?.get(position)?.fechaInicioClub)
          //  var urlVideo =clubs?.get(position)?.urlPromocionClub

            // val previewVideoUrl = Uri.parse(urlVideo)


           textNameProducto.text = producto?.get(position)?.producto_nombreProducto.toString()
           textdescripcionProducto.text = producto?.get(position)?.producto_detalleProducto.toString()
            textprecioProducto.text = producto?.get(position)?.producto_precioProducto.toString()
            Picasso.get().load(producto?.get(position)?.producto_thumbProducto.toString()).into(imgProducto);
            //   textfechaInicioClub.text ="INICIA "+ fechaInicio






        }


    }
}
