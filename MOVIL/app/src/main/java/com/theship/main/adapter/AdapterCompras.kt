package com.theship.main.adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.theship.R
import com.theship.model.club.comprasTotal
import android.content.SharedPreferences
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.theship.ui.carrito.CarritoFragment
import com.theship.ui.suscripcion.SuscripcionFragment
import okhttp3.internal.notify


class AdapterCompras(var producto: ArrayList<comprasTotal>?) :
    RecyclerView.Adapter<AdapterCompras.RecyclerViewHolder>() {


    override fun getItemCount(): Int {
        return producto!!.size

    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindData(producto, position)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        var view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.reciclerviewmiscompras,
            parent,
            false
        )

        return RecyclerViewHolder(view)
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textNameProducto: TextView = itemView.findViewById(R.id.nombreClubCompra)
        var textdescripcionProducto: TextView = itemView.findViewById(R.id.descripcionClubCompra)
        var opponentImage: ImageView = itemView.findViewById(R.id.imgClubcompra)
        var buttonELinminar: Button = itemView.findViewById(R.id.dropCompra)


        fun bindData(producto: ArrayList<comprasTotal>?, position: Int) {

            textNameProducto.text = producto?.get(position)?.nombreClub.toString()
            textdescripcionProducto.text = producto?.get(position)?.descripcionClub.toString()
            Picasso.get().load(producto?.get(position)?.urlImagenClub.toString())
                .into(opponentImage);


            buttonELinminar.setOnClickListener {
                val settings: SharedPreferences =
                    itemView.context.getSharedPreferences("COMPRAS", Context.MODE_PRIVATE)
           val removeSt=settings.edit().remove(producto?.get(position)?.nombreClub.toString()).commit()

            if (removeSt == true){
                producto?.removeAt(position)
                producto?.clear()
                

            }

            }


        }

    }
}
