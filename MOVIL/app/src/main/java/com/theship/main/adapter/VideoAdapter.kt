package com.theship.main.adapter

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.VideoView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.theship.R
import com.theship.model.contenido.Contenido


class  VideoAdapter(var contenidos: ArrayList<Contenido>?) : RecyclerView.Adapter<VideoAdapter.RecyclerViewHolder>() {

    var context: Context? = null

    override fun getItemCount(): Int {
        return contenidos!!.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bindData(contenidos, position)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        var view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.recyclerview_main,
            parent,
            false
        )

        return RecyclerViewHolder(view)
    }

    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
      var urlVideo: VideoView = itemView.findViewById(R.id.videoFreeSuscripcion);
        var textTitle: TextView = itemView.findViewById(R.id.titleVideo)
        var descripcion: TextView = itemView.findViewById(R.id.descripcionVideo)
        var nombreCreador: TextView = itemView.findViewById(R.id.nombreCreator)
        var imgUser: ImageView = itemView.findViewById(R.id.profile_image)
       val progressBar:ProgressBar=itemView.findViewById(R.id.progressBar)
        //  var opponentImage: TextView = itemView.findViewById(R.id.opponent_image)

        fun bindData(contenidos: ArrayList<Contenido>?, position: Int) {

        //    var fech = SimpleDateFormat("EEE dd MMMM yyyy")
          //  var fechaInicio = fech.format(clubs?.get(position)?.fechaInicioClub)
           // var urlVideo =clubs?.get(position)?.urlPromocionClub

            val previewVideoUrl = contenidos?.get(position)?.contenido_sources


            val video = Uri.parse(previewVideoUrl)
            urlVideo.setVideoURI(video)
            // urlVideo.setVideoPath(contenidos?.get(position)?.contenido_sources.toString())
            urlVideo.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp: MediaPlayer? ->
                mp?.start()
                progressBar.visibility = View.GONE
                val videoRatio = mp!!.videoWidth / mp!!.videoHeight.toFloat()
                val screenRatio = urlVideo.width / urlVideo.height.toFloat()
                val scale = videoRatio / screenRatio
                if (scale >= 1f) {
                    urlVideo.scaleX = scale
                    //urlVideo.setScaleX(scale);

                } else {
                    urlVideo.scaleY = 1f / scale
                    //urlVideo.setScaleY(1f/scale)
                }


            })
            urlVideo.setOnCompletionListener(MediaPlayer.OnCompletionListener { mp: MediaPlayer? ->
                mp?.start()

            })
            textTitle.text = contenidos?.get(position)?.contenido_title.toString()
            descripcion.text = contenidos?.get(position)?.contenido_description.toString()
            nombreCreador.text = contenidos?.get(position)?.user_name.toString()
            Picasso.get().load(contenidos?.get(position)?.user_photoUser.toString()).into(imgUser);



            //textNameClub.setOnClickListener {




            //}




        }


    }

}