package com.theship.main

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.theship.R
import com.theship.databinding.FragmentRegistroBinding
import com.theship.viewmodel.RegistroViewModel


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class RegistroFragment : Fragment() {

    private lateinit var viewModelRegistro: RegistroViewModel
    private lateinit var binding: FragmentRegistroBinding
    private val myContext: FragmentActivity? = null
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModelRegistro =
            ViewModelProvider(this).get(RegistroViewModel::class.java)
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_registro, container, false

        )
        val view: View = binding.getRoot()

        viewModelRegistro = ViewModelProvider(this).get()
        binding.viewModelRegistro = viewModelRegistro
        binding.lifecycleOwner = this


        with(binding) {

            registrar.setOnClickListener {
                viewModelRegistro?.onButtonClickedRegister(
                    nombre?.text.toString(),
                    apellidos?.text.toString(),
                    fechaNacimiento?.text.toString(),
                    whatsapp?.text.toString(),
                    email?.text.toString(),
                    passwordLogin?.text.toString(),
                    password?.text.toString()
                )
            }
        }


            activity?.let {
                viewModelRegistro.getOkMutable().observe(it, Observer {
                    val intent = Intent(activity, Suscripcion::class.java)
                    startActivity(intent)

                })
            }
        return view
    }

}