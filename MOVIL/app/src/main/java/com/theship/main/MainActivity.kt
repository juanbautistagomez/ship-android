package com.theship.main


import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.theship.R
import com.theship.databinding.ActivityMainBinding

import com.theship.viewmodel.LoginViewModel

class MainActivity : AppCompatActivity() {


    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: ActivityMainBinding
    private var token: String? = null
     private var id: Int? = null
     private var chk_recordar: CheckBox? = null
      private lateinit var bottonNavegation: BottomNavigationItemView
     private val SETTINGS = "tokenRefresh"
     private val SETTINGS1 = "suscripcion"
    lateinit var buttonStart: Button
    lateinit var buttonregister: Button
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.containerFragment, HomeFreeVideoFragment())
        transaction.disallowAddToBackStack()
        transaction.commit()
        existToken()

        buttonStart = findViewById(R.id.buttonLogin)
        buttonregister = findViewById(R.id.buttonregister)
        buttonStart.setOnClickListener {
            val intent = Intent(this, Login::class.java)
            intent.putExtra("login", "login" )
            // startActivity(intent)
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        }

        buttonregister.setOnClickListener{
            val intent = Intent(this, Login::class.java)
            intent.putExtra("login", "register" )
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        }

       with(binding) {

          /*  navigationView.setOnNavigationItemReselectedListener (BottomNavigationView.OnNavigationItemReselectedListener {
               if ( it.itemId== R.id.freeVideo){
                   val transaction = supportFragmentManager.beginTransaction()
                   transaction.replace(R.id.containerFragment, HomeFreeVideoFragment())
                   transaction.disallowAddToBackStack()
                   transaction.commit()
               }
                if ( it.itemId== R.id.freeVideo1){
                    val transaction = supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.containerFragment, ProductoFragment())
                    transaction.disallowAddToBackStack()
                    transaction.commit()
                }
                if ( it.itemId== R.id.freeVideo2){
                    val transaction = supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.containerFragment, LoginFragment())
                    transaction.disallowAddToBackStack()
                    transaction.commit()
                }
            })*/

        }

    }



    private fun obtenerToken(): String {
        val settings = getSharedPreferences(SETTINGS, 0)
        val token = settings.getString("tokenRefresh", "")
        return token.toString()
    }
    private fun obtenerSuscripcion(): String {
        val settings = getSharedPreferences(SETTINGS1, 0)
        val suscripcion = settings.getString("suscripcion", "")
        return suscripcion.toString()
    }

    fun existToken(): Boolean {
        if (obtenerToken() == "") {
          /*  val intent = Intent(this, Apertura::class.java)
            startActivity(intent)*/
            return false
        } else if(obtenerSuscripcion() == "false"){
            val intent = Intent(this, Suscripcion::class.java)
            startActivity(intent)
           return true
        }/*else{
            val intent = Intent(this, DrawerMenu::class.java)
            startActivity(intent)
            return true
        }*/else if(obtenerSuscripcion() =="true"){
            val intent = Intent(this, DrawerMenu::class.java)
            startActivity(intent)
            return true
        }
        return false
    }
}

//getPresenter().inject(this, getValidateInternet())
//  toolbar = supportActionBar!!
/*
        existToken()
        email = findViewById(R.id.email_login)
        password = findViewById(R.id.password_login)

        val btn_entrar = findViewById(R.id.login) as Button
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(getString(R.string.entrando))

        btn_entrar.setOnClickListener {
            logIn()
        }
        chk_recordar = findViewById(R.id.chk_recordar) as CheckBox
        val recordarMail: Boolean = Preferencias().get(this)!!.getBoolean("recordar", false)
        chk_recordar!!.isChecked = recordarMail
        if (recordarMail) {
            email?.setText(Preferencias().get(this)!!.getString("email", "email"))
        }
        /* var mainFragment : LoginFragment = LoginFragment()
         supportFragmentManager.beginTransaction().add(R.id.container, mainFragment!!)
             .commit()*/

    }

    fun getValidateInternet(): IValidateInternet? {
        return validateInternet
    }

    override fun onStop() {
        super.onStop()
        finish()
    }

    override fun onStart() {
        super.onStart()


    }

    override fun onPause() {
        super.onPause()
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        finish()
    }

    fun logIn() {
// Resetear errores

        // Resetear errores
        email?.setError(null)
        password?.setError(null)

        val email: String = email?.getText().toString()
        val password: String = password?.getText().toString()

        login(email, password)

    }

    fun login(email: String, password: String) {
        val login = UserLogin(email, password)

        val call: Call<TokenResponse?>? = ApiCliente.getApiService()?.login(login)
        call?.enqueue(object : Callback<TokenResponse?> {
            override fun onResponse(
                    call: Call<TokenResponse?>,
                    response: Response<TokenResponse?>,
            ) {
                if (response.isSuccessful) {
                    val token = response.body()?.getTokenRefresh()

                    tokenRefresh(token!!)
                    // guardarToken(token!!)

                    // irMain(token!!)

                } else {
                    Toast.makeText(this@MainActivity, "Login incorrecto !", Toast.LENGTH_SHORT)
                            .show()
                    //    progressDialog!!.cancel()
                    //Toast.makeText("", "Login incorrecto !", Toast.LENGTH_SHORT)
                    //.show()
                }
            }

            override fun onFailure(call: Call<TokenResponse?>, t: Throwable) {
                //  progressDialog!!.cancel()

                Toast.makeText(this@MainActivity, t.message + "error :(", Toast.LENGTH_SHORT)
                        .show()
            }
        })

    }

    fun tokenRefresh(tokenRefresh: String) {
        val call: Call<TokenResponse?>? = ApiCliente.getApiService()?.tokenRefresh(tokenRefresh)
        call?.enqueue(object : Callback<TokenResponse?> {
            override fun onResponse(
                    call: Call<TokenResponse?>,
                    response: Response<TokenResponse?>
            ) {
                if (response.isSuccessful) {
                    val token = response.body()?.getToken()
                    id = response.body()?.getId()
                    guardarToken(token.toString());
                    progressDialog!!.cancel()
                    if (obtenerToken() == token) {
                        irMain()
                    } else {
                        return
                    }
                }
                Toast.makeText(this@MainActivity, "Bienvenido !", Toast.LENGTH_SHORT)
                        .show()
            }

            override fun onFailure(call: Call<TokenResponse?>, t: Throwable) {
                Toast.makeText(this@MainActivity, t.message + "error :(", Toast.LENGTH_SHORT)
                        .show()
            }

        });

    }

    private fun irMain() {
        val intent = Intent(this, Suscripcion::class.java)
        startActivity(intent)
    }

    private fun guardarToken(token: String) {
        val settings = getSharedPreferences(SETTINGS, 0)
        val editor = settings.edit()
        editor.putString("auth", token)
        editor.commit()
    }

    private fun obtenerToken(): String {
        val settings = getSharedPreferences(SETTINGS, 0)
        val token = settings.getString("auth", "")
        return token.toString()
    }

fun existToken(): Boolean {
        if (obtenerToken() == "") {
            return false
        } else {
            irMain()
            return true
        }
    }*/

