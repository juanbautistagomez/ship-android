package com.theship.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.viewpager2.widget.ViewPager2
import com.google.gson.Gson
import com.theship.R
import com.theship.api.ApiCliente
import com.theship.main.adapter.VideoAdapter
import com.theship.model.contenido.Contenido
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class HomeFreeVideoFragment : Fragment() {
    var contenidos: ArrayList<Contenido> = ArrayList()
    private lateinit var recycler: ViewPager2

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment


        val token = "auth"

        val call: Call<ArrayList<Contenido?>>? = ApiCliente.getApiService()?.getContenidoGratuito(token)
        call?.enqueue(object : Callback<ArrayList<Contenido?>> {
            override fun onFailure(call: Call<ArrayList<Contenido?>>?, t: Throwable) {
                var error = t.message.toString()
            }

            override fun onResponse(
                    call: Call<ArrayList<Contenido?>>?,
                    response: Response<ArrayList<Contenido?>>?,
            ) {
                if (response != null) {
                    if (response.isSuccessful) {
                        var dat = response.body()

//convierte a random los resultados antes de enviar al adaptador
                        Collections.shuffle(dat)

                        contenidos = (dat as ArrayList<Contenido>?)!!
                        recycler.adapter = VideoAdapter(dat as ArrayList<Contenido>?)

                        //*  val res = arrayListOf()

                        //      Toast.makeText(this@DrawerMenu, res.toArray().toString(), Toast.LENGTH_SHORT)
                        //        .show()
                        Log.i("hola lista", Gson().toJson(dat))

                    }
                }

            }
        })
        return inflater.inflate(R.layout.fragment_home_videofree, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler = view.findViewById(R.id.VideoFreeRecycler) as ViewPager2
        // recycler.layoutManager = LinearLayoutManager(activity)
        recycler.addItemDecoration(DividerItemDecoration(activity,
                DividerItemDecoration.VERTICAL))


    }
}