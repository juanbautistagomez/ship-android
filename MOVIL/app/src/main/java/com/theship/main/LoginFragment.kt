package com.theship.main

import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.theship.R

import com.theship.databinding.FragmentLoginBinding
import com.theship.viewmodel.LoginViewModel
import androidx.fragment.app.FragmentActivity


class LoginFragment : Fragment() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: FragmentLoginBinding

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        viewModel =
            ViewModelProvider(this).get(LoginViewModel::class.java)
         binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_login, container, false

        )

        val view: View = binding.getRoot()
        viewModel = ViewModelProvider(this).get()
       binding.viewModel = viewModel
        binding.lifecycleOwner = this

        with(binding) {

            login.setOnClickListener {
                viewModel?.onButtonClicked(
                    emailLogin.text.toString(),
                    passwordLogin.text.toString()
                )
            }

            registroApp.setOnClickListener {
                val transaction = (activity as FragmentActivity).supportFragmentManager.beginTransaction()
                transaction.replace(R.id.container, RegistroFragment())
                transaction.disallowAddToBackStack()
                transaction.commit()
            }
        }


        activity?.let {
            viewModel.getOkMutable().observe(it, Observer {
if (it==false){
    val intent = Intent(activity, Suscripcion::class.java)
    intent.putExtra("tokenRefresh", viewModel.tokenValido)
    intent.putExtra("suscripcion","false")
    //  intent.putExtra("id",    viewModel.idUser)
    startActivity(intent)
}else {

    val intent = Intent(activity, DrawerMenu::class.java)
    intent.putExtra("tokenRefresh", viewModel.tokenValido)
    intent.putExtra("suscripcion", viewModel.suscripcion)
    //  intent.putExtra("id",    viewModel.idUser)
    startActivity(intent)
    // startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle())
}
            })
        }

        return view
    }
}