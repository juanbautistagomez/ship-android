package com.theship.main

import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentActivity
import com.theship.R
import org.jetbrains.anko.startActivityForResult

class Apertura : AppCompatActivity() {
    lateinit var buttonStart:Button
    lateinit var buttonregister:Button
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apertura)
        buttonStart = findViewById(R.id.buttonLogin)
        buttonregister = findViewById(R.id.buttonregister)
        buttonStart.setOnClickListener {
            val intent = Intent(this, Login::class.java)
            intent.putExtra("login", "login" )
           // startActivity(intent)
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        }

        buttonregister.setOnClickListener{
            val intent = Intent(this, Login::class.java)
            intent.putExtra("login", "register" )
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        }
    }
}