package com.theship.main

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.theship.R
import com.theship.api.ApiCliente
import com.theship.main.adapter.AdapterClub
import com.theship.main.adapter.AdapterProducto
import com.theship.model.club.Club
import com.theship.model.club.Producto
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ProductoFragment : Fragment() {
    val EXECUTION_TIME: Long = 60 // 60 segundos (60 * 1000 millisegundos)
    private val SETTINGS = "auth"
    private val handler: Handler? = null
    private val runnable: Runnable? = null


    var clubs: ArrayList<Club> = ArrayList()
    private lateinit var recycler: RecyclerView

    //producto
    var productos: ArrayList<Producto> = ArrayList()
    private lateinit var recyclerProducto: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_suscripcion_club, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       // recycler = view.findViewById(R.id.pagoSuscripcion) as RecyclerView
//        recycler.layoutManager = LinearLayoutManager(activity)
  //      recycler.addItemDecoration(DividerItemDecoration(activity,
       //         DividerItemDecoration.VERTICAL))

        //producto
     //   recyclerProducto = view.findViewById(R.id.productosComprar) as RecyclerView
      //  recyclerProducto.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL,false )

      //  recyclerProducto.addItemDecoration(DividerItemDecoration(activity,
                //          DividerItemDecoration.VERTICAL))


    //    obtenerDatos()
    //    obtenerProducto()

    }
    fun obtenerDatos() {

       // val settings = getSharedPreferences(SETTINGS, 0)
        val token ="auth"

        val call: Call<ArrayList<Club?>>? = ApiCliente.getApiService()?.getClubs()
        call?.enqueue(object : Callback<ArrayList<Club?>> {
            override fun onFailure(call: Call<ArrayList<Club?>>?, t: Throwable) {
                var error = t.message.toString()
            }

            override fun onResponse(
                    call: Call<ArrayList<Club?>>?,
                    response: Response<ArrayList<Club?>>?,
            ) {
                if (response != null) {
                    if (response.isSuccessful) {
                        var dat = response.body()


                        /*     clubs = (dat as ArrayList<Club>?)!!
                        recycler.adapter = AdapterClub(dat as ArrayList<Club>?)
*/
                        //*  val res = arrayListOf()

                        //      Toast.makeText(this@DrawerMenu, res.toArray().toString(), Toast.LENGTH_SHORT)
                        //        .show()
                        Log.i("hola lista", Gson().toJson(dat))

                    }
                }

            }
        })

    }

    fun obtenerProducto(){
        // val settings = getSharedPreferences(SETTINGS, 0)
        val token ="auth"

        val call: Call<ArrayList<Producto?>>? = ApiCliente.getApiService()?.getProductos(token)
        call?.enqueue(object : Callback<ArrayList<Producto?>> {
            override fun onFailure(call: Call<ArrayList<Producto?>>?, t: Throwable) {
                var error = t.message.toString()
            }

            override fun onResponse(
                    call: Call<ArrayList<Producto?>>?,
                    response: Response<ArrayList<Producto?>>?,
            ) {
                if (response != null) {
                    if (response.isSuccessful) {
                        var dat = response.body()


                        productos = (dat as ArrayList<Producto>?)!!
                        recyclerProducto.adapter = AdapterProducto(dat as ArrayList<Producto>?)

                        //*  val res = arrayListOf()

                        //      Toast.makeText(this@DrawerMenu, res.toArray().toString(), Toast.LENGTH_SHORT)
                        //        .show()
                        Log.i("hola lista", Gson().toJson(dat))

                    }
                }

            }
        })
    }
}