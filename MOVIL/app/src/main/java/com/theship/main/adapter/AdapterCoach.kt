package com.theship.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso
import com.theship.R
import com.theship.model.user.User


class AdapterCoach : BaseAdapter {
    var playerList = ArrayList<User>()
    var context: Context? = null

    constructor(context: Context, playersList: ArrayList<User>) : super() {
        this.context = context
        this.playerList = playersList
    }

    override fun getCount(): Int {
        return playerList.size
    }

    override fun getItem(position: Int): Any {
        return playerList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val plyer = this.playerList[position]

        var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var playerView = inflator.inflate(R.layout.grid_coach, null)
        val nombre = playerView.findViewById<TextView>(R.id.nombreCoach)
        val nombreRol = playerView.findViewById<TextView>(R.id.nombreRol)
        val imageCoach = playerView.findViewById<ImageView>(R.id.imagenCoach)

        playerView.setOnClickListener {
Toast.makeText(context,"Funciona",Toast.LENGTH_LONG).show()
        }
        nombre.text = plyer?.name!!
        nombreRol.text = plyer?.nombreRole!!


        //playerView.nombrePlayer.text = plyer?.name!!
        //playerView.tipoPlayer.text = plyer?.position!!
        Picasso.get().load(plyer.photoUser).into(imageCoach)

        return playerView
    }
}
