package com.theship.main.adapter

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.theship.R
import com.theship.main.Suscripcion
import com.theship.model.club.Club
import com.theship.model.club.ListaCompra
import com.theship.model.user.User
import retrofit2.Callback
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class AdapterClub : BaseAdapter {
    var COMPRAS = "COMPRAS"
    var playerList = ArrayList<Club>()
    var context: Context? = null

    constructor(context: Context, playersList: ArrayList<Club>) : super() {
        this.context = context
        this.playerList = playersList
    }


    override fun getCount(): Int {
        return playerList.size
    }

    override fun getItem(position: Int): Any {
        return playerList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val plyer = this.playerList[position]

        var inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var playerView = inflator.inflate(R.layout.recycler_suscripcion, null)
        val nombre = playerView.findViewById<TextView>(R.id.nombreClub)
        val img = playerView.findViewById<ImageView>(R.id.imgClub)
        val descripcion = playerView.findViewById<TextView>(R.id.descripcionClub)

        val addSuscripcion = playerView.findViewById<TextView>(R.id.addSuscripcion)

        img.setOnClickListener {
            Toast.makeText(context, "Funciona", Toast.LENGTH_LONG).show()
        }

        addSuscripcion.setOnClickListener {
            Toast.makeText(context, "Has añadido al carito", Toast.LENGTH_LONG).show()

            val idclub = plyer?.idclub!!.toString()
            val nombreClub = plyer?.nombreClub!!
            val urlImagenClub = plyer?.urlImagenClub!!

            if (plyer.wellnescombo == false){
                borrarCompras()
            }

            var arr = arrayListOf<String>(idclub + "{" + nombreClub + "{" + urlImagenClub + "{" + plyer?.descripcionClub!!)
            var compras = TreeSet<String>(arr)

            guardarCompras(compras, plyer?.nombreClub!!)

        }
        nombre.text = plyer?.nombreClub!!
        descripcion.text = plyer?.descripcionClub!!

        addSuscripcion.text = "Agregar al carito"

        //playerView.tipoPlayer.text = plyer?.position!!
        Picasso.get().load(plyer.urlImagenClub).into(img)

        return playerView
    }



    private fun guardarCompras(compras: TreeSet<String>?, key: String?) {
        val settings = context?.getSharedPreferences(COMPRAS, 0)
        val editor = settings?.edit()
        editor?.putString(key, compras.toString())
        editor?.apply()
    }

    private fun borrarCompras(){
        val settings: SharedPreferences =
            context?.getSharedPreferences("COMPRAS", Context.MODE_PRIVATE)!!
        settings.edit().clear().commit()

    }


}
