package com.theship.main

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.GridView
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.theship.R
import com.theship.api.ApiCliente
import com.theship.main.adapter.AdapterClub
import com.theship.model.club.Club
import com.theship.model.club.ListaCompra
import com.theship.ui.carrito.CarritoFragment
import com.theship.ui.suscripcion.SuscripcionFragment
import com.theship.viewmodel.LoginViewModel
import com.theship.viewmodel.SuscripcionViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.min

class Suscripcion : AppCompatActivity() {
    private val SETTINGS = "tokenRefresh"
    private val SETTINGS1 = "suscripcion"
    private val COMPRAS = "COMPRAS"
    private  var token:String? = null
    private  var suscripcion:String? = null

   private lateinit var carrito:TextView
   private lateinit var contador:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_suscripcion)
       // setSupportActionBar(findViewById(R.id.toolbar))



        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.nav_host_compra, SuscripcionFragment())
        transaction.disallowAddToBackStack()
        transaction.commit()

        carrito= findViewById(R.id.carrito)
        contador= findViewById(R.id.contador)
        obtenerCompras()
        carrito.setOnClickListener {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.nav_host_compra, CarritoFragment())
            transaction.disallowAddToBackStack()
            transaction.commit()
            obtenerCompras()
        }


        val intent = intent
        token = intent.getStringExtra("tokenRefresh")
        suscripcion = intent.getStringExtra("suscripcion")

       existToken(token,suscripcion)
        // receiver = AirplaneModeChangeReceiver()
        IntentFilter(Intent.ACTION_POWER_CONNECTED).also {

        }



    }
    private fun obtenerCompras(): String {
        val settings = this.getSharedPreferences(COMPRAS, 0)
        val body = settings?.getString("BODY", null)
        val mind = settings?.getString("MIND", null)
        val nutrition = settings?.getString("NUTRICIÓN ", null)
        val talleres = settings?.getString("TALLERES ", null)
        val reto = settings?.getString("RETO 21 DIAS ", null)

        if (body !=null || mind !=null || nutrition !=null || talleres !=null ){

            val arra= mutableListOf<String>()

                if (body != null || body == "") {
                    arra.add(body)
                    arra.remove(reto)
                }
                if (nutrition != null || nutrition == "") {
                    arra.add(nutrition)
                    arra.remove(reto)
                }
                if (mind != null || mind =="") {
                    arra.add(mind)
                    arra.remove(reto)
                }
                if (talleres != null) {
                    arra.add(talleres)
                    arra.remove(reto)
                }
                if (reto != null || reto =="") {
                    arra.remove(body)
                    arra.remove(mind)
                    arra.remove(talleres)
                    arra.remove(nutrition)
                    arra.add(reto)
            }

                contador.text=   arra.size.toString()

        }
        return body.toString()
    }

        private fun guardarToken(token: String?) {
        val settings = getSharedPreferences(SETTINGS, 0)
        val editor = settings.edit()
        editor.putString("tokenRefresh", token)
        editor.apply()
    }

    private fun guardarSuscripcion(suscripcion:String?) {
        val settings = getSharedPreferences(SETTINGS1, 0)
        val editor = settings.edit()
        editor.putString("suscripcion", suscripcion)
        editor.apply()
    }
    fun existToken(token:String?,suscripcion:String?) {
        guardarToken(token)
        guardarSuscripcion(suscripcion)
    }



}