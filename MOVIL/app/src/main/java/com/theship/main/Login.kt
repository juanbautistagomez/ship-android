package com.theship.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.theship.R
import com.theship.databinding.FragmentLoginBinding
import com.theship.viewmodel.LoginViewModel

class Login : AppCompatActivity() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: FragmentLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val intent = intent
        var action= intent.getStringExtra("login")

        if (action =="login"){
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, LoginFragment())
            transaction.disallowAddToBackStack()
            transaction.commit()
        } else if(action=="register"){
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, RegistroFragment())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }




    }
}