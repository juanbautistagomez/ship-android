package com.theship.main

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import com.google.android.material.navigation.NavigationView
import com.theship.R
import com.theship.api.ApiCliente
import com.theship.model.login.TokenResponse
import com.theship.ui.actividades.ActividadesFragment
import com.theship.ui.coach.CoachFragment
import com.theship.ui.home.HomeFragment
import com.theship.ui.slideshow.SlideshowFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DrawerMenu : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var   navView: NavigationView
    private  var token:String? = null
    private var tokenValido:String?=null
    var idUser: Int = 0
             var id:Int = 0
    private val SETTINGS = "tokenRefresh"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer_menu)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val intent = intent
        token = intent.getStringExtra("tokenRefresh")

        existToken()

       //  id = intent.getIntExtra("id",0)


        drawerLayout = findViewById(R.id.drawer_layout)
       navView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)

       val toggle:ActionBarDrawerToggle= ActionBarDrawerToggle(
           this,
           drawerLayout,
           toolbar,
           R.string.next,
           R.string.title_activity_drawer_menu
       )
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)
    }

    //refresca el token para consultar usando el token refresh
    fun tokenRefresh(tokenRefresh: String?) {
        val call: Call<TokenResponse?>? = ApiCliente.getApiService()?.tokenRefresh(tokenRefresh.toString())
        call?.enqueue(object : Callback<TokenResponse?> {
            override fun onResponse(
                call: Call<TokenResponse?>,
                response: Response<TokenResponse?>
            ) {
                if (response.isSuccessful) {
                      tokenValido = response.body()?.getToken()
                    //  var token3 = tokenValido
                     idUser = response.body()?.getId()!!

                }
                   Toast.makeText(this@DrawerMenu, "Se ha generado un nuevo  token   !", Toast.LENGTH_SHORT).show()
            }

            override fun onFailure(call: Call<TokenResponse?>, t: Throwable) {
                //  oKmutable.value = false
            }
        })
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val transaction = supportFragmentManager.beginTransaction()
        when(item.itemId){
            R.id.nav_actividades -> {
                val fragment: ActividadesFragment = ActividadesFragment()

                transaction.replace(R.id.nav_host_fragment, fragment)
                transaction.disallowAddToBackStack()
                transaction.commit()


            }
         /*   R.id.nav_slideshow -> {
                val fragment: SlideshowFragment = SlideshowFragment()
                tokenRefresh(obtenerToken())
                val args = Bundle()
                args.putInt("id", idUser)
                args.putString("token", tokenValido)

                transaction.replace(R.id.nav_host_fragment, fragment)
                fragment.setArguments(args)
                transaction.disallowAddToBackStack()
                transaction.commit()


            }*/
            R.id.nav_home -> {
                val fragment: HomeFragment = HomeFragment()
                tokenRefresh(obtenerToken())
                val args = Bundle()
                args.putInt("id",idUser)
                args.putString("token", tokenValido)

                transaction.replace(R.id.nav_host_fragment, fragment)
                fragment.setArguments(args)
                transaction.disallowAddToBackStack()
                transaction.commit()


            }
            R.id.nav_coaches -> {
                val fragment: CoachFragment = CoachFragment()
                tokenRefresh(obtenerToken())
                val args = Bundle()
                args.putInt("id",idUser)
                args.putString("token", tokenValido)

                transaction.replace(R.id.nav_host_fragment, fragment)
                fragment.setArguments(args)
                transaction.disallowAddToBackStack()
                transaction.commit()


            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    // se guarda el token refresh en share preference
    private fun guardarToken(token: String?) {
        val settings = getSharedPreferences(SETTINGS, 0)
        val editor = settings.edit()
        editor.putString("tokenRefresh", token)
      //  editor.putInt("id", id)
        editor.apply()
    }

    //ontenemos el token en share preference
    private fun obtenerToken(): String {
        val settings = getSharedPreferences(SETTINGS, 0)
        val token = settings.getString("tokenRefresh", "")
        return token.toString()

    }
    fun existToken(): Boolean {
        if (obtenerToken() != "") {
            tokenRefresh(obtenerToken())
            return false
        } else {
            guardarToken(token)
            return true
        }
    }


   }