package com.theship.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.theship.model.user.User

@Dao
interface UserDAO {
    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    fun insert (user:User)

    @Query("SELECT * FROM user WHERE id = :id")
    fun findByLogin(id:String):LiveData<User>







}