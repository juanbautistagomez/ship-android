package com.theship.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.theship.model.Token.Token
import com.theship.model.user.User

@Database(
 entities = [
     User::class,
     Token::class
 ],
    version = 1
)

abstract class TheShiftDb: RoomDatabase(){
    abstract fun userDao():UserDAO
    abstract fun tokenDao():TokenDAO

}