package com.theship.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.theship.model.Token.Token
import com.theship.model.user.User


@Dao
interface TokenDAO{
    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    fun insert (token: Token)

    @Query("SELECT * FROM token WHERE tokenRefresh = :tokenRefresh")
    fun getToken(tokenRefresh:String): LiveData<Token>

}
