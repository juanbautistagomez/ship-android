
import { Router } from 'express';
import { TipoPagoController} from "../controllers/TipoPagoController";

const router = Router();
router.get("/", TipoPagoController.getAllTipoPago);
router.get("/:idtipopago",TipoPagoController.getByIdTipoPago);
router.post("/",TipoPagoController.createTipoPago);
router.put("/:idtipopago",TipoPagoController.updateTipoPago);
router.delete("/:idtipopago",TipoPagoController.deleteTipoPago);

export default router;