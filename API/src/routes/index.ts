import { Router } from 'express';
import user from './user';
import auth from './auth';
import role from './role';
import modulo from './modulo';
import club from './club';
import suscripcion from './suscripcion';
import pago from './pago';
import tipopago from './tipopago';
import contenido from './contenido';
import producto from './producto';
import coach from './coach';
import suscriptores from './suscriptores';
import precioclub from './precioclub';



const routes = Router();

routes.use('/auth', auth);
routes.use('/users', user);
routes.use('/role', role);
routes.use('/modulo', modulo);
routes.use('/club',club);
routes.use('/suscripcion',suscripcion)
routes.use('/pago',pago)
routes.use('/tipopago',tipopago)
routes.use('/contenido',contenido)
routes.use('/producto',producto)
routes.use('/coach',coach)
routes.use('/suscriptores',suscriptores)
routes.use('/precioclub',precioclub)

export default routes;