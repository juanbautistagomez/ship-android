
import { Router } from 'express';
import { ClubController} from "../controllers/ClubController";
import { checkJwt } from '../middlewares/jwt';


const router = Router();
router.get("/" ,ClubController.getClub);
router.get("/:idclub",ClubController.getByIdClub);
router.post("/",[(checkJwt)],ClubController.createClub);
router.put("/:idclub",[(checkJwt)],ClubController.updateClub);
router.delete("/:idclub",ClubController.deleteClub);

export default router;