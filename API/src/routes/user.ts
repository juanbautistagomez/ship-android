
import { Router } from 'express';
import { UserController} from "../controllers/UserController";
import { checkRole } from './../middlewares/role';
import { checkJwt } from './../middlewares/jwt';

const router = Router();
router.get("/",[checkJwt], UserController.getUsers);
router.get("/:id",[checkJwt],UserController.getUser);
router.post("/",UserController.createUser);
router.put("/:id",UserController.updateUser);
router.delete("/:id",UserController.deleteUser);



export default router;