import { Router } from 'express';
import { ModuloController} from "../controllers/ModuloController";

const router = Router();
router.get("/", ModuloController.getModulos);
router.get("/:idmodulo",ModuloController.getByIdModulo);
router.post("/",ModuloController.createModulo);
router.put("/:idmodulo",ModuloController.updateModulo);
router.delete("/:idmodulo",ModuloController.deleteModulo);

export default router;