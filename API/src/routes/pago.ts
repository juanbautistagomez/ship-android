
import { Router } from 'express';
import { PagoController} from "../controllers/PagoController";

const router = Router();
router.get("/", PagoController.getAllPago);
router.get("/:idpago",PagoController.getByIdPago);
router.post("/",PagoController.createPago);
router.put("/:idpago",PagoController.updatePago);
router.delete("/:idpago",PagoController.deletePago);

export default router;