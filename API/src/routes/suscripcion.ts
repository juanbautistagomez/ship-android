
import { Router } from 'express';
import { SuscripcionController} from "../controllers/SuscripcionController";

const router = Router();
router.get("/", SuscripcionController.getAllSuscripcion);
router.get("/:idsuscripcion",SuscripcionController.getByIdSuscripcion);
router.post("/",SuscripcionController.createSuscripcion);
router.put("/:idsuscripcion",SuscripcionController.updateSuscripcion);
router.delete("/:idsuscripcion",SuscripcionController.deleteSuscripcion);

export default router;