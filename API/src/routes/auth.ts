import { checkJwt } from './../middlewares/jwt';
import { Router } from 'express';
import AuthController from '../controllers/AuthController';

const router = Router();

// login
router.post('/login', AuthController.login);

// Change password
router.post('/change-password', [checkJwt], AuthController.changePassword);

// refresh token
router.post('/refresh', AuthController.refreshToken);

export default router;
