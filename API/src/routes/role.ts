
import { Router } from 'express';
import { RoleController} from "../controllers/RoleController";

const router = Router();
router.get("/", RoleController.getRole);
router.get("/:idrole",RoleController.getByIdRole);
router.post("/",RoleController.createRole);
router.put("/:idrole",RoleController.updateRole);
router.delete("/:idrole",RoleController.deleteRole);

export default router;