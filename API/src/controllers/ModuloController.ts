import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Modulo } from "../entity/Modulo";
import { validate } from 'class-validator';

export  class ModuloController {

  static getModulos =  async (req: Request, res: Response) => {
    const moduloRepository = getRepository(Modulo);
    let role;

    try {
      role = await moduloRepository.createQueryBuilder('Modulo')
      .getMany();
    } catch (e) {
      res.status(404).json({ message: 'Ha ocurrido un error inesperado!' });
    }

    if (role.length > 0) {
      res.send(role);
    } else {
      res.status(404).json({ message: 'Ningún resultado' });
    }
  };

  static getByIdModulo = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const results = await getRepository(Modulo).findOne(req.params.id);
    return res.json(results);
  };

  static createModulo = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const {  email, password,name,updateAt,direccion,edad,ocupacion,sexo,telefono} = req.body;
    const modulo = new Modulo()
   /* user.email = email;
    user.password = password;
    user.role = role;
    user.updateAt=updateAt;
    user.name= name;*/
  
    const validationOpt = { validationError: { target: false, value: false } };
    const errors = await validate(modulo, validationOpt);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    const moduloRepository = getRepository(Modulo);
    try {
   
      await moduloRepository.save(modulo);
    } catch (e) {
      return res.status(409).json({ message: 'Usuario ya existe' });
    }

    return res.send('Usuario creado exitosamente')
  };

  static updateModulo = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const modulo = await getRepository(Modulo).findOne(req.params.id);
    if (modulo) {
      getRepository(Modulo).merge(modulo, req.body);
      const results = await getRepository(Modulo).save(modulo);
      return res.json(results);
    }

    return res.json({ msg: 'Not user found' });
  };

  static deleteModulo = async (req: Request, res: Response): Promise<Response> => {
    const results = await getRepository(Modulo).delete(req.params.id);
    return res.json(results);
  };
}