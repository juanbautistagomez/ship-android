
import { getRepository } from 'typeorm';
import { Request, Response } from 'express';
import { validate } from 'class-validator';
import { Suscripcion } from '../entity/Suscripcion';



export class SuscripcionController {

    static getAllSuscripcion = async (req: Request, res: Response) => {
        const suscripcionRepository = getRepository(Suscripcion);
        let suscripcion;

        try {
            suscripcion = await suscripcionRepository.createQueryBuilder('suscripcion')
                //.innerJoinAndSelect('clientes.proveedor','proveedor')
                .getMany();
        } catch (e) {
            res.status(404).json({ message: 'Ha ocurrido un error inesperado!' });
        }

        if (suscripcion.length > 0) {
            res.send(suscripcion);
        } else {
            res.status(404).json({ message: 'Ningún resultado' });
        }
    }
    static createSuscripcion = async (req: Request, res: Response) => {
     
        const {nombreSuscripcion,detalleSuscripcion,condicionesSuscripcion } = req.body;
      
        const suscripcion = new Suscripcion();
        // Try get user
        suscripcion.nombreSuscripcion = nombreSuscripcion;
        suscripcion.detalleSuscripcion = detalleSuscripcion;
        suscripcion.condicionesSuscripcion = condicionesSuscripcion;

        const validationOpt = { validationError: { target: false, value: false } };
        const errors = await validate(suscripcion, validationOpt);
        const suscripcionRepository = getRepository(Suscripcion);
        if (errors.length > 0) {
          return res.status(400).json(errors);
        }
    
        // Try to save user
        try {
          await suscripcionRepository.save(suscripcion);
        } catch (e) {
          return res.status(409).json({ message: 'Ubo un error' });
        }
    
        res.status(201).json({ message: 'Suscripcion Creado' });

    }

    static getByIdSuscripcion = async (req: Request, res: Response) => {
        const suscripcionRepository = getRepository(Suscripcion);
        const { idsucripcion } = req.params;
        try {
            const suscripcion = await suscripcionRepository.findOneOrFail(idsucripcion)
               
            res.send(suscripcion);
        } catch (e) {
            res.status(404).json({ message: 'Ningún resultado' });
        }

    }
    static updateSuscripcion = async (req: Request, res: Response) => {
       
    const { idsuscripcion } = req.params;
    const suscripcion = await getRepository(Suscripcion).findOne(idsuscripcion);
    if (suscripcion) {
        const {nombreSuscripcion,detalleSuscripcion,condicionesSuscripcion } = req.body;
      
      const suscripcionRepository = getRepository(Suscripcion);
      // Try get user
      try {
        suscripcion.nombreSuscripcion = nombreSuscripcion;
        suscripcion.detalleSuscripcion = detalleSuscripcion;
        suscripcion.condicionesSuscripcion = condicionesSuscripcion;
              
      } catch (e) {
        return res.status(404).json({ message: 'Opss .....!! not found' });
      }
      const validationOpt = { validationError: { target: false, value: false } };
      const errors = await validate(suscripcion, validationOpt);
  
      if (errors.length > 0) {
        return res.status(400).json(errors);
      }else{
        await suscripcionRepository.save(suscripcion);
      }
     
      // Try to save user
      try {
        await suscripcionRepository.save(suscripcion);
      } catch (e) {
        return res.status(409).json({ message: 'Suscripcion en uso actualmente' });
      }
      res.status(201).json({ message: 'Suscripcion actualizado' });
    }
    res.status(400).json({ message: 'No existe' });
    }

    static deleteSuscripcion = async (req: Request, res: Response): Promise<Response> => {
        const { idsuscripcion } = req.params;
        const clientesRepository = getRepository(Suscripcion);
        let clientes: Suscripcion;

        try {
            clientes = await clientesRepository.findOneOrFail(idsuscripcion);
          } catch (e) {
            return res.status(404).json({ message: 'Suscripcion no existe' });
          }
          clientesRepository.delete(idsuscripcion)
          res.status(201).json({ message: ' Suscripcion eliminado' });
      };

}

export default SuscripcionController;
