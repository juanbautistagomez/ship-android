import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Role } from "../entity/Role";
import { validate } from 'class-validator';

export  class RoleController {

  static getRole =  async (req: Request, res: Response) => {
    const userRepository = getRepository(Role);
    let role;

    try {
      role = await userRepository.createQueryBuilder('Role')
      .getMany();
    } catch (e) {
      res.status(404).json({ message: 'Ha ocurrido un error inesperado!' });
    }

    if (role.length > 0) {
      res.send(role);
    } else {
      res.status(404).json({ message: 'Ningún resultado' });
    }
  };

  static getByIdRole = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const results = await getRepository(Role).findOne(req.params.idrole);
    return res.json(results);
  };

  static createRole = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const {  email, password,name,updateAt,direccion,edad,ocupacion,sexo,telefono} = req.body;
    const role = new Role()
   /* user.email = email;
    user.password = password;
    user.role = role;
    user.updateAt=updateAt;
    user.name= name;*/
  
    const validationOpt = { validationError: { target: false, value: false } };
    const errors = await validate(role, validationOpt);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    const userRepository = getRepository(Role);
    try {
   
      await userRepository.save(role);
    } catch (e) {
      return res.status(409).json({ message: 'Usuario ya existe' });
    }

    return res.send('Usuario creado exitosamente')
  };

  static updateRole = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const { idrole } = req.params;
    const role = await getRepository(Role).findOne(idrole);
    if (role) {
      const {nombreRole} = req.body;
      const rolRepository = getRepository(Role);
        try{
          role.nombreRole= nombreRole;
         // getRepository(Role).merge(role, req.body);
        }catch{
          return res.status(404).json({ message: 'Opss .....!! not found' });
        
        }
        const validationOpt = { validationError: { target: false, value: false } };
        const errors = await validate(role, validationOpt);
        if (errors.length > 0) {
          return res.status(400).json(errors);
        }
        try{
          
           await rolRepository.save(role);
          
        }catch{
          return res.status(409).json({ message: 'Rol no se pudo actualizar ' });
        
        }
     // return res.json(results);
    }

    res.status(201).json({ message: 'Rol actualizado' });
  };

  static deleteRole = async (req: Request, res: Response): Promise<Response> => {
    
    const results = await getRepository(Role).delete(req.params.idrole);
    return res.json(results);
  };
}