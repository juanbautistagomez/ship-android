import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
import { validate } from 'class-validator';

export  class UserController {

  static getUsers =  async (req: Request, res: Response) => {
    const userRepository = getRepository(User);
    let users;

    try {
      users = await userRepository.createQueryBuilder('user')
     
      .getMany();
    } catch (e) {
      res.status(404).json({ message: 'Ha ocurrido un error inesperado!' });
    }

    if (users.length > 0) {
      res.send(users);
    } else {
      res.status(404).json({ message: 'Ningún resultado' });
    }
  };

  static getUsersCoaches =  async (req: Request, res: Response) => {
    const userRepository = getRepository(User);
    let users;

    try {
      users = await userRepository.query(`SELECT U.name, U.apellidos, U.photoUser,E.nombreRole
      FROM user U
      JOIN role E on E.idrole = u.roleIdrole where u.roleIdrole !=1  `)
    } catch (e) {
      res.status(404).json({ message: 'Ha ocurrido un error inesperado!' });
    }

    if (users.length > 0) {
      res.send(users);
    } else {
      res.status(404).json({ message: 'Ningún resultado' });
    }
  };


  static getUser = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const userRepository = getRepository(User);
    let results = await userRepository.createQueryBuilder("user")
    .where('user.id=:id',{id:req.params.id})
    .leftJoinAndSelect('user.role','role')
    .getMany()
 
    return res.json(results);
  };

  static createUser = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const {  email, password, role,name,apellidos,photoUser,fechaNacimiento,ocupacion,sexo,telefono} = req.body;
    const user = new User()
    user.email = email;
    user.photoUser = photoUser
    user.password = password;
    user.role = role;
    user.name= name;
    user.apellidos= apellidos;
    user.fechaNacimiento = fechaNacimiento;
  
    const validationOpt = { validationError: { target: false, value: false } };
    const errors = await validate(user, validationOpt);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    const userRepository = getRepository(User);
    try {
      user.hashPassword();
      await userRepository.save(user);
    } catch (e) {
      return res.status(409).json({ message: 'Usuario ya existe' });
    }

   // return res.send('Usuario creado exitosamente')
    return res.status(200).json({message:"Usuario Creado exitosamente"})
    };

  static updateUser = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const user = await getRepository(User).findOne(req.params.id);
    if (user) {
      getRepository(User).merge(user, req.body);
      const results = await getRepository(User).save(user);
      return res.json(results);
    }

    return res.json({ msg: 'Not user found' });
  };

  static deleteUser = async (req: Request, res: Response): Promise<Response> => {
    const results = await getRepository(User).delete(req.params.id);
    return res.json(results);
  };
}