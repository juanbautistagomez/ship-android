
import { getRepository } from 'typeorm';
import { Request, Response } from 'express';
import { validate } from 'class-validator';
import { TipoPago } from '../entity/TipoPago';



export class TipoPagoController {

    static getAllTipoPago = async (req: Request, res: Response) => {
        const pagoRepository = getRepository(TipoPago);
        let tipopago;

        try {
            tipopago = await pagoRepository.createQueryBuilder('tipo_pago')
                //.innerJoinAndSelect('clientes.proveedor','proveedor')
                .getMany();
        } catch (e) {
            res.status(404).json({ message: 'Ha ocurrido un error inesperado!' });
        }

        if (tipopago.length > 0) {
            res.send(tipopago);
        } else {
            res.status(404).json({ message: 'Ningún resultado' });
        }
    }
    static createTipoPago = async (req: Request, res: Response) => {
     
        const { nombreTipoPago} = req.body;
      
        const tipopago = new TipoPago();
        // Try get pago
     tipopago.nombreTipoPago=nombreTipoPago
        const validationOpt = { validationError: { target: false, value: false } };
        const errors = await validate(tipopago, validationOpt);
        const tipopagoRepository = getRepository(TipoPago);
        if (errors.length > 0) {
          return res.status(400).json(errors);
        }
    
        // Try to save user
        try {
          await tipopagoRepository.save(tipopago);
        } catch (e) {
          return res.status(409).json({ message: 'Ubo un error' });
        }
    
        res.status(201).json({ message: 'Tipo de pago Creado' });

    }

    static getByIdTipoPago = async (req: Request, res: Response) => {
        const pagoRepository = getRepository(TipoPago);
        const { idtipopago } = req.params;
        try {
            const tipopago = await pagoRepository.findOneOrFail(idtipopago)
               
            res.send(tipopago);
        } catch (e) {
            res.status(404).json({ message: 'Ningún resultado' });
        }

    }
    static updateTipoPago = async (req: Request, res: Response) => {
       
    const { idtipopago } = req.params;
    const tipopago = await getRepository(TipoPago).findOne(idtipopago);
    if (tipopago) {
        const {nombreTipoPago } = req.body;
      
      const pagoRepository = getRepository(TipoPago);
      // Try get user
      try {
       tipopago.nombreTipoPago=nombreTipoPago;
              
      } catch (e) {
        return res.status(404).json({ message: 'Opss .....!! not found' });
      }
      const validationOpt = { validationError: { target: false, value: false } };
      const errors = await validate(tipopago, validationOpt);
  
      if (errors.length > 0) {
        return res.status(400).json(errors);
      }else{
        await pagoRepository.save(tipopago);
      }
     
      // Try to save user
      try {
        await pagoRepository.save(tipopago);
      } catch (e) {
        return res.status(409).json({ message: 'Tipo Pago en uso actualmente' });
      }
      res.status(201).json({ message: 'Tipo Pago actualizado' });
    }
    res.status(400).json({ message: 'No existe' });
    }

    static deleteTipoPago = async (req: Request, res: Response): Promise<Response> => {
        const { idtipopago } = req.params;
        const tipopagoRepository = getRepository(TipoPago);
        let tipopago: TipoPago;

        try {
            tipopago = await tipopagoRepository.findOneOrFail(idtipopago);
          } catch (e) {
            return res.status(404).json({ message: 'Tipo pago no existe' });
          }
          tipopagoRepository.delete(idtipopago)
          res.status(201).json({ message: ' Tipo Pago eliminado' });
      };

}

export default TipoPagoController;
