import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Club } from "../entity/Club";
import { validate } from 'class-validator';

export  class ClubController {

  static getClub =  async (req: Request, res: Response) => {
    const clubRepository = getRepository(Club);
    let club;

    try {
      club = await clubRepository.createQueryBuilder('Club')
      .getMany();
    } catch (e) {
      res.status(404).json({ message: 'Ha ocurrido un error inesperado!' });
    }

    if (club.length > 0) {
      res.send(club);
    } else {
      res.status(404).json({ message: 'Ningún resultado' });
    }
  };

  static getByIdClub = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const results = await getRepository(Club).findOne(req.params.idrole);
    return res.json(results);
  };

  static createClub = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const {  nombreClub,fechaInicioClub,descripcionClub} = req.body;
    const club = new Club()
   
   club.nombreClub=nombreClub;
   club.descripcionClub = descripcionClub;
   club.fechaInicioClub = fechaInicioClub;
    const validationOpt = { validationError: { target: false, value: false } };
    const errors = await validate(club, validationOpt);
    if (errors.length > 0) {
      return res.status(400).json(errors);
    }
    const userRepository = getRepository(Club);
    try {
   
      await userRepository.save(club);
    } catch (e) {
      return res.status(409).json({ message: 'Club ya existe' });
    }

    return res.send('Club creado exitosamente')
  };

  static updateClub = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const { idclub } = req.params;
    const club = await getRepository(Club).findOne(idclub);
    if (club) {
      const {nombreClub,descripcionClub,fechaInicioClub} = req.body;
      const clubRepository = getRepository(Club);
        try{
        club.nombreClub= nombreClub;        
        club.descripcionClub = descripcionClub;        
        club.fechaInicioClub = fechaInicioClub;
         // getRepository(Role).merge(role, req.body);
        }catch{
          return res.status(404).json({ message: 'Opss .....!! not found' });
        
        }
        const validationOpt = { validationError: { target: false, value: false } };
        const errors = await validate(club, validationOpt);
        if (errors.length > 0) {
          return res.status(400).json(errors);
        }
        try{
          
           await clubRepository.save(club);
          
        }catch{
          return res.status(409).json({ message: 'Club no se pudo actualizar ' });
        
        }
     // return res.json(results);
    }

    res.status(201).json({ message: 'Rol actualizado' });
  };

  static deleteClub = async (req: Request, res: Response): Promise<Response> => {
    
    const results = await getRepository(Club).delete(req.params.idclub);
    return res.json(results);
  };
}