import { getRepository, Index, JoinColumn, JoinTable } from 'typeorm';
import { query, Request, Response } from 'express';
import { User } from '../entity/User';
import * as jwt from 'jsonwebtoken';
import config from '../config/config';
import { validate } from 'class-validator';

class AuthController {
  static login = async (req: Request, res: Response) => {
    const { email, password } = req.body;

    if (!(email && password)) {
      return res.status(400).json({ message: ' Correo & Contraseña es requerido!' });
    }

    const userRepository = getRepository(User);
    let user: User;

    try {
      user = await userRepository.findOneOrFail({ where: { email } });
    } catch (e) {
      return res.status(400).json({ message: ' Correo electronico o contraseña incorecta!' });
    }

    // Check password
    if (!user.checkPassword(password)) {
      return res.status(400).json({ message: 'El correo o la contraseña es incorrecta!' });
    }

    const token = jwt.sign({ userId: user.id, email: user.email }, config.jwtSecret, { expiresIn: '120' });
    const tokenRefresh = jwt.sign({ userId: user.id, email: user.email }, config.jwtSecretRefresh, { expiresIn: '730001h' });

    user.refreshToken = tokenRefresh
    try {
      await userRepository.save(user)
    } catch (error) {
      return res.status(400)
    }
    res.json({

      message: 'Usuario Registrado', token, tokenRefresh: tokenRefresh
    }); // Devuelve la data del usuario 


  }

  static changePassword = async (req: Request, res: Response) => {
    const { userId } = res.locals.jwtPayload;
    const { oldPassword, newPassword } = req.body;

    if (!(oldPassword && newPassword)) {
      res.status(400).json({ message: 'Se requieren contraseña antigua y contraseña nueva' });
    }

    const userRepository = getRepository(User);
    let user: User;

    try {
      user = await userRepository.findOneOrFail(userId);
    } catch (e) {
      res.status(400).json({ message: 'Uppss... Algo salio mal!!' });
    }

    if (!user.checkPassword(oldPassword)) {
      return res.status(401).json({ message: 'Verifique  tu contraseña anterior' });
    }

    user.password = newPassword;
    const validationOps = { validationError: { target: false, value: false } };
    const errors = await validate(user, validationOps);

    if (errors.length > 0) {
      return res.status(400).json(errors);
    }

    // Hash password
    user.hashPassword();
    userRepository.save(user);

    res.json({ message: 'Cambio de contraseña!' });
  }

  static refreshToken = async (req: Request, res: Response) => {

    const refreshToken = req.headers.refresh as string;
    if (!(refreshToken)) {
      res.status(400).json({ message: 'Somenthing  goes wrong!' });
    }
    const userRepository = getRepository(User);
    let user: User;

    try {
      const verifyResult = jwt.verify(refreshToken, config.jwtSecretRefresh);
      const { email } = verifyResult as User;

      user = await userRepository.findOneOrFail({ where: { email } });

    } catch (error) {
      return res.status(400).json({ mesagge: 'Somenthing  goes wrong!' })

    }

    let role;

    role = await userRepository.
      createQueryBuilder('user')
      .leftJoinAndSelect('user.role', 'role')
      .where('user.id=:id', { id: user.id })
      .select('role')
      .getRawMany()


    const token = jwt.sign({
      userId: user.id, name: user.name, email: user.email, id: user.id,
      role: role, createdAt: user.createdAt, updateAt: user.updateAt
    }, config.jwtSecret, { expiresIn: '50s' })
    res.json({ message: 'OK', token,id:user.id });
  };
}

export default AuthController;
