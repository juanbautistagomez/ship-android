
import { getRepository } from 'typeorm';
import { Request, Response } from 'express';
import { validate, MinLength } from 'class-validator';
import { Pago } from '../entity/Pago';


export class PagoController {

    static getAllPago = async (req: Request, res: Response) => {
        const pagoRepository = getRepository(Pago);
        let pago;

        try {
            pago = await pagoRepository.createQueryBuilder('pago')
                //.innerJoinAndSelect('clientes.proveedor','proveedor')
                .getMany();
        } catch (e) {
            res.status(404).json({ message: 'Ha ocurrido un error inesperado!' });
        }

        if (pago.length > 0) {
            res.send(pago);
        } else {
            res.status(404).json({ message: 'Ningún resultado' });
        }
    }
    static createPago = async (req: Request, res: Response) => {

        const { montoPago, fechaPago, club, user, suscripcion, formaPago } = req.body;
        const pago = new Pago();
        const pagoRepository = getRepository(Pago);
      
        if (user) {
            
            const userExist = await pagoRepository.query(`select  pago.statusPago from pago where userid = ${user} and statusPago=true and idpago=( Select MAX(idpago) from pago );`);
                      
            if ( userExist[0] && userExist[0].statusPago ==true  ) {
              return res.status(409).json({ message: 'Al parecer tu suscripcion aun esta activa',userExist});                          

            } else {
             
                    // Try get pago
                    pago.montoPago = montoPago;
                    pago.club = club;
                    pago.user = user;
                    pago.suscripcion = suscripcion;
                    pago.tipopago = formaPago;
                    pago.statusPago = true;

                    const validationOpt = { validationError: { target: false, value: false } };
                    const errors = await validate(pago, validationOpt);
                    const pagoRepository = getRepository(Pago);
                    if (errors.length > 0) {
                        return res.status(400).json(errors);
                    }

                    // Try to save user
                    try {
                        await pagoRepository.save(pago);
                    } catch (e) {
                        return res.status(409).json({ message: 'Ubo un error' });
                    }

                    res.status(201).json({ message: 'Pago Exitoso',pago });


            }
        }



    }

    static getByIdPago = async (req: Request, res: Response) => {
        const pagoRepository = getRepository(Pago);
        const { idpago } = req.params;
        try {
            const pago = await pagoRepository.findOneOrFail(idpago)

            res.send(pago);
        } catch (e) {
            res.status(404).json({ message: 'Ningún resultado' });
        }

    }
    static updatePago = async (req: Request, res: Response) => {

        const { idpago } = req.params;
        const pago = await getRepository(Pago).findOne(idpago);
        if (pago) {
            const { montoPago, fechaPago, club, user, suscripcion, tipoPago } = req.body;

            const pagoRepository = getRepository(Pago);
            // Try get user
            try {
                pago.montoPago = montoPago;
                pago.club = club;
                pago.user = user;
                pago.suscripcion = suscripcion;
                pago.tipopago = tipoPago;

            } catch (e) {
                return res.status(404).json({ message: 'Opss .....!! not found' });
            }
            const validationOpt = { validationError: { target: false, value: false } };
            const errors = await validate(pago, validationOpt);

            if (errors.length > 0) {
                return res.status(400).json(errors);
            } else {
                await pagoRepository.save(pago);
            }

            // Try to save user
            try {
                await pagoRepository.save(pago);
            } catch (e) {
                return res.status(409).json({ message: 'Pago en uso actualmente' });
            }
            res.status(201).json({ message: 'Pago actualizado' });
        }
        res.status(400).json({ message: 'No existe' });
    }

    static deletePago = async (req: Request, res: Response): Promise<Response> => {
        const { idpago } = req.params;
        const pagoRepository = getRepository(Pago);
        let pago: Pago;

        try {
            pago = await pagoRepository.findOneOrFail(idpago);
        } catch (e) {
            return res.status(404).json({ message: 'Pago no existe' });
        }
        pagoRepository.delete(idpago)
        res.status(201).json({ message: ' Pago eliminado' });
    };

}

export default PagoController;
