import { Entity, Column, PrimaryGeneratedColumn, OneToMany, Unique,ManyToMany } from 'typeorm';
import {Pago} from './Pago';
import {Suscriptores} from './Suscriptores';

@Unique(['fechaInicioClub'])
@Entity()
export class Club {
  @PrimaryGeneratedColumn()
  idclub: number;

  @Column()
  nombreClub: string;

  @Column("longtext",{nullable:true})
  descripcionClub: string;

  @Column("longtext",{nullable:true})
  urlPromocionalClub: string;

  @Column("longtext",{nullable:true})
  urlImagenClub: string;

  @Column({nullable:true})
  fechaInicioClub: Date;
  
  //Un club tendra muchos pagos
  @OneToMany(() => Pago, pago => pago.user)
  pago: Pago[];

//Un club tendra muchos registros
@OneToMany(() => Suscriptores, suscriptores => suscriptores.club)
suscriptores: Suscriptores[];

}