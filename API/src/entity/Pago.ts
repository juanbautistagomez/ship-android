import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, Unique, Generated, CreateDateColumn } from 'typeorm';

import  {User} from '../entity/User'
import  {Club} from '../entity/Club'
import { Suscripcion } from './Suscripcion';
import { TipoPago } from './TipoPago';

@Entity()
export class Pago {
  @PrimaryGeneratedColumn()
  idpago: number;

 
  @Column()
  montoPago:number;

  @Column()
  @CreateDateColumn()
  fechaPago:Date;

  @Column()
  statusPago:boolean;

  //Un usuario tendra muchos pagos
     @ManyToOne(() => User, user => user.pago)
    user: User;

    //un club tendra muchos pagos
    @ManyToOne(() => Club, club => club.pago)
    club: Club;

    //una suscripcion tendra muchos pagos
    @ManyToOne(() => Suscripcion, suscripcion => suscripcion.pago)
    suscripcion: Suscripcion;

     //una pago tendra varios tipos de pago pagos
     @ManyToOne(() => TipoPago, tipopago => tipopago.pago)
     tipopago: TipoPago;
}
