import {Entity, Column, PrimaryGeneratedColumn,OneToMany} from 'typeorm'


@Entity()
export class Programa {
  @PrimaryGeneratedColumn()
  idprograma: number;

  @Column()
  nombrePrograma: string;

  @Column()
  fecha: Date;

  @Column({nullable:true})
  tipoPrograma: string;


@Column()
detallePrograma:string;
}