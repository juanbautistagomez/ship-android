import { Entity, Column, PrimaryGeneratedColumn, OneToMany, Unique } from 'typeorm';
import {Pago} from './Pago';

@Entity()
export class Suscripcion {
  @PrimaryGeneratedColumn()
  idsuscripcion: number;

  @Column()
  nombreSuscripcion: string;

  @Column('longtext',{nullable:true})
  detalleSuscripcion: string;

  @Column('longtext',{nullable:true})
  condicionesSuscripcion: string;

  //Un club tendra muchos pagos
  @OneToMany(() => Pago, pago => pago.user)
  pago: Pago[];

}