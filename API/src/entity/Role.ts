import {Entity, Column, PrimaryGeneratedColumn,ManyToMany,JoinTable,OneToMany,Unique} from 'typeorm'

import { User } from './User';
@Unique(['nombreRole'])
@Entity()
export class Role {
  @PrimaryGeneratedColumn()
  idrole: number;

  @Column()
  nombreRole: string;

  


 @OneToMany(()  => User, user => user.role)
 user: User[];
}