import {Entity, Column, PrimaryGeneratedColumn,OneToMany} from 'typeorm'


@Entity()
export class Yoga{
    @PrimaryGeneratedColumn()
    idyoga:number;

    @Column()
    nombreYoga:string;

    @Column()
    detallesYoga:string;

    @Column()
    duracionYoga:string;


}