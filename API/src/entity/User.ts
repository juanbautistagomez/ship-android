import { Entity, Column, PrimaryGeneratedColumn, Unique, ManyToOne, OneToMany, CreateDateColumn, ManyToMany,JoinTable } from 'typeorm';
import {Role} from './Role';
import {Pago} from './Pago';
import * as bcrypt from 'bcryptjs';
import { MinLength, IsNotEmpty, IsEmail,IsEmpty, IsOptional } from 'class-validator';
import { Contenido } from './Contenido';
import { Suscriptores } from './Suscriptores';
@Entity()
@Unique(['email'])
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty({ message: 'The name is required' })
  @MinLength(3)
  name: string;

  @Column({nullable:true})
  apellidos: string;

  @Column({nullable:true})
  fechaNacimiento: string;


  @Column("longtext",{nullable:true})
  photoUser: string;

  @Column()
  @IsNotEmpty({ message: 'The email is required' })
  @IsEmail({}, { message: 'Incorrect email' })
  email: string;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @IsNotEmpty({ message: 'The password is required' })
  password:string;

  @Column({nullable:true})
  @IsOptional()
  @IsNotEmpty({ message: 'token is required' })
  refreshToken:string;

  @Column()
  @CreateDateColumn()
  updateAt:Date;

   // role se cambio por otra tabla
   /*@ManyToOne(() => Role, roles => roles.user,{cascade:true})
   role: Role;*/
   
   @ManyToOne(() => Role, role => role.user,{cascade:true})
   role: Role;

//Un usuario tendra muchos pagos
@OneToMany(() => Pago, pago => pago.user)
pago: Pago[];

//Un usuario tendra muchos registros
@OneToMany(() => Suscriptores, suscriptores => suscriptores.user)
suscriptores: Suscriptores[];

@OneToMany(() => Contenido, contenido => contenido.user)
contenido: Contenido[];

  hashPassword(): void {
    const salt = bcrypt.genSaltSync(10);
    this.password = bcrypt.hashSync(this.password, salt);
  }

  checkPassword(password: string): boolean {
    return bcrypt.compareSync(password, this.password);
  }

  
}