import { Entity, Column, PrimaryGeneratedColumn, OneToMany, Unique } from 'typeorm';


@Entity()
export class TipoContenido {
  @PrimaryGeneratedColumn()
  idtipoContenido: number;

  @Column()
  nombreTipoContenido: string;

  
}