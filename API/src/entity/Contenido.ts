import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne } from 'typeorm';
import { User } from './User';

@Entity()
export class Contenido {
  @PrimaryGeneratedColumn()
  idcontenido:number;

  @Column()
  title:string;

  @Column({nullable:true})
  subtitle:string;

  @Column('longtext',{nullable:true})
  description:string;

  @Column('longtext',{nullable:true})
  thumb:string;


  @Column()
    gratuito:boolean;

  @Column('longtext',{nullable:true})
  sources:string;

  @CreateDateColumn()
  fechaCreated:Date;

  //Un usuario tendra muchos pagos
  @ManyToOne(() => User, user => user.contenido)
  user: User;
  
}
