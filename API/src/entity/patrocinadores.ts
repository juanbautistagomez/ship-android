import {Entity, Column, PrimaryGeneratedColumn,ManyToOne,JoinColumn,ManyToMany,} from 'typeorm'


@Entity()
export class Patrocinador {
  @PrimaryGeneratedColumn()
  idpatrocinador: number;

  @Column({nullable:true})
  nombrePatrocinador: string;

  @Column()
  logoPatrocinador: string;

  @Column({nullable:true})
  webUrlPatrocinador: string;



}