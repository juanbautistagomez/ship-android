import { Entity, Column, PrimaryGeneratedColumn, Unique, ManyToOne, OneToMany, CreateDateColumn, ManyToMany } from 'typeorm';
import {Role} from './Role';
import {Pago} from './Pago';
import * as bcrypt from 'bcryptjs';
import { MinLength, IsNotEmpty, IsEmail,IsEmpty, IsOptional } from 'class-validator';
@Entity()

export class Coaches {
  @PrimaryGeneratedColumn()
  idcoaches: number;

  @Column()
  @IsNotEmpty({ message: 'The name is required' })
  @MinLength(3)
  nameCoaches: string;

  @Column({nullable:true})
  apellidosCoaches: string;

 

  @Column()
  @CreateDateColumn()
  createdAt: Date;
 

  
}