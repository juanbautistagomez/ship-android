import { Entity, Column, PrimaryGeneratedColumn, OneToMany, Unique } from 'typeorm';
import {Pago} from './Pago';

@Entity()
export class TipoPago {
  @PrimaryGeneratedColumn()
  idtipoPago: number;

  @Column()
  nombreTipoPago: string;
//Un club tendra muchos pagos
@OneToMany(() => Pago, pago => pago.tipopago)
pago: Pago[];
  
}