import {Entity, Column, PrimaryGeneratedColumn,OneToMany} from 'typeorm'
import { Role } from './Role';


@Entity()
export class Modulo {
  @PrimaryGeneratedColumn()
  idmodulo: number;

  @Column()
  nombreModulo: string;

 

}