import { getRepository } from 'typeorm';
import { Request, Response, NextFunction } from 'express';
import { User } from '../entity/User';
import { Role} from '../entity/role';

export const checkRole = (roles: Array<string>) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    const { userId } = res.locals.jwtPayload;
    const userRepository = getRepository(User);
    let user: User;

    try {
      user = await userRepository.findOneOrFail(userId);
    } catch (e) {
      return res.status(401).json({ message: 'Not Authorized' });
    }

    //Check
    const rolRepository = getRepository(Role);
      let rol:Role;
         // Validate
         
         rol = await rolRepository.createQueryBuilder('roles')
         .leftJoinAndSelect('roles.users', 'users')
         .where('users.id=:userId',{userId:userId})
         .getOne()
       
if(rol){
  const { nombreRole } = rol;
  if (roles.includes(nombreRole)) {
    next();
  } else {
    res.status(401).json({ message: 'Not Authorized' });
  }
}else{
  return res.status(401).json({ message: 'Not Authorized' });
}
   
  };
};
